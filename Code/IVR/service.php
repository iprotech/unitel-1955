#!/usr/bin/php -q
<?php
require('lib/phpagi.php');
require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');
require('lib/config112014.php');
require('lib/config01062014.php');

$agi = new AGI();
$msisdn = $agi->get_variable('CALLERID(num)', TRUE);

$mysql = new Mysql($db);

$info = $agi->get_variable('info', TRUE);
$dataArray = explode('-', $info);

$memberId = $dataArray[0];
$serviceId = $dataArray[1];
$msisdn = $dataArray[2];

//$agi->say_digits($memberId);
//$msisdn='2099774888';

if(!$memberId||!$serviceId||!$msisdn){
    //echo $msisdn;
    $msisdn = $agi->get_variable("EXTEN",true);
    //$msisdn='2099774888';
    $dataArray = $mysql->ExecuteQuery("SELECT s.id, s.member_id, m.msisdn, s.msc FROM service AS s INNER JOIN member AS m ON s.member_id = m.id
    WHERE s.status = 1 AND m.msisdn='{$msisdn}'" );
    //var_dump($dataArray);
    if($dataArray){
        $memberId = $dataArray[0]['member_id'];
        $serviceId = $dataArray[0]['id'];
        $msisdn = $dataArray[0]['msisdn'];
    }
}

//$agi->say_digits($memberId);

$mysql->ExecuteQuery("UPDATE service SET answer_time = now() WHERE member_id = '" . $memberId."'");

$count = $mysql->Select('question', 'COUNT(id) AS questionCount', 'status = 1 AND convert_status = 1');
if (!$count || $count[0]['questionCount'] == 0) {
	$mysql->Disconnect();
    $agi->exec_goto('quiz2-main-menu', $exten, 1);
	return;
}

$cache = array();
$where = 'status = 1 AND convert_status = 1';

$agi->stream_file($system_file['begin_service']);

$index = 0;

$skipCount = 0;
while ($index < 5 && $skipCount < 2) {
    if (!$sessionId) $sessionId = time();
    $whereIn = $where;
    if (count($cache) > 0) {
        $whereIn .= ' AND status=1 AND id NOT IN (' . implode(', ', $cache) . ')';
    }
    $result = $mysql->Select('question', 'id, file_path_question, file_path_choose, answer_key', $whereIn, '', 'RAND()', '1');

    if (!$result) {
        $cache = array();
        continue;
    }
    $question = $result[0];
    $cache[] = $question['id'];
    $keyTrue = $question['answer_key'];
    $i = 0;
    while ($i < 2) {
        $break = FALSE;
        $is_ask =  false;
        $agi->stream_file($quizPath . getFileName($question['file_path_question']));
        $input = $agi->get_data($quizPath . getFileName($question['file_path_choose']), $timeout, 1);

        switch ($input['result']) {
            case '0':
		$i++;
		continue;
                break;
            case '6':
                $break = TRUE;
                break;
            case '':
		$skipCount++;
                break;
            case $keyTrue:
                if(true) {
                    $is_ask = false;
                    $agi->stream_file($system_file['answer_true']);
                    $mysql->ExecuteQuery('UPDATE member SET score = score + 1, new_score = new_score + 1 WHERE id = ' . $memberId);
                    $mysql->Insert('member_score', array('member_id', 'score', 'created_datetime', 'score_type', 'session_id','question_id'), array($memberId, 1, date('Y-m-d H:i:s', time()), 2, "{$sessionId}","{$question['id']}"));

                    /* Check Exchange */
                    $TT_Score = $mysql->Select('member', 'new_score', 'msisdn="' . $msisdn . '"', '', '', '1');

                    if (!$TT_Score) exit;
                    if ($Total_Score['new_score'] == '20') {
                        $exchange = 10;
                        $is_ask = TRUE;
                    } else if ($Total_Score['new_score'] == '30') {
                        $exchange = 20;
                        $is_ask = TRUE;
                    } else if ($Total_Score['new_score'] == '40') {
                        $exchange = 30;
                        $is_ask = TRUE;
                    }else if ($Total_Score['new_score'] >= '60') {
                        $exchange = 50;
                        $is_ask = TRUE;
                        $exchange = 50;
                        $sql_exchange = 'INSERT INTO score_exchange(member_id, msisdn, score) 
                                                    VALUES(' . $memberId . ', "' . $msisdn . '", "' . $exchange . '")';
                        $mysql->ExecuteQuery($sql_exchange);

                        $sql_emptyscore = 'UPDATE member SET new_score=0 WHERE msisdn="' . $msisdn . '"';
                        $mysql->ExecuteQuery($sql_emptyscore);

                        $LastID = $mysql->Select('score_exchange', 'id', 'member_id=' . $memberId, '', 'created_datetime DESC', '1');
                        $sql_updateREF = 'UPDATE member_score SET exchange_id = ' . $LastID[0]['id'] . ' WHERE exchange_id=0 && member_id=' . $memberId;
                        $mysql->ExecuteQuery($sql_updateREF);
						$agi->stream_file($system_file['exchange_' . $exchange]);
                        break;
                    }

                    if ($is_ask) {
                        $j = 1;
                        while (1) {
                            if ($j++ > 3) {
                                $agi->stream_file($system_file['more_exchange']);
                                break;
                            }

                            $inputConfirm = $agi->get_data($systemPath . 'exchange_' . $exchange);
                            switch ($inputConfirm['result']) {
                                case 1:
                                    $sql_exchange = 'INSERT INTO score_exchange(member_id, msisdn, score) 
                                                    VALUES(' . $memberId . ', "' . $msisdn . '", "' . $exchange . '")';
                                    $mysql->ExecuteQuery($sql_exchange);

                                    $sql_emptyscore = 'UPDATE member SET new_score=0 WHERE msisdn="' . $msisdn . '"';
                                    $mysql->ExecuteQuery($sql_emptyscore);

                                    $LastID = $mysql->Select('score_exchange', 'id', 'member_id=' . $memberId, '', 'id DESC', '1');
                                    $sql_updateREF = 'UPDATE member_score SET exchange_id = ' . $LastID[0]['id'] . ' WHERE exchange_id=0 && member_id=' . $memberId;
                                    $mysql->ExecuteQuery($sql_updateREF);
                                    $is_confirm = TRUE;
				    $agi->stream_file($system_file['confirm_exchange']);
                                    break;
                                case 2:
                                    $agi->stream_file($system_file['more_exchange']);
                                    $is_confirm = TRUE;
                                    break;
                            }

                            if ($is_confirm)
                                break;
                        }
                    }
                    
                    $skipCount = 0;
                    $break = TRUE;
                    break;
                } else {
                    $mysql->ExecuteQuery("UPDATE member SET score = score + 1 WHERE id = '" . $memberId . "'");
                    $mysql->Insert('member_score', array('member_id', 'score', 'created_datetime', 'score_type'), array($memberId, 1, date('Y-m-d H:i:s', time()), '2'));
                    $agi->stream_file($system_file['answer_true']);
                    $break = TRUE;
                    $skipCount = 0;
                    //$index++;
                    break;
                }
            default:
                $agi->stream_file($system_file['answer_false']);
                $break = TRUE;
		$skipCount = 0;
                //$index++;
                break;
        }
        if ($break)
            break;
		$i+=2;
    } 
    $index++;
}
$agi->stream_file($system_file['end_service']);
$mysql->Disconnect();
$agi->hangup();
?>

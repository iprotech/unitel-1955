<?php


function getFileName($name) {
    return substr($name, 0, strrpos($name, '.'));
}

function pushLog($title, $msg) {
    global $logFile;
    $content = "{$title}: {$msg}\n";
    
    if (!file_exists($logFile)) {
        file_put_contents($logFile, $content);
    } else {
        $f = fopen($logFile, 'a');
        fwrite($f, $content);
        fclose($f);
    }
}

?>

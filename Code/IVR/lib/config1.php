<?php

error_reporting(FALSE);

$exten = 1955;
$timeout = 5000;

$db = array();
$db[0]['host'] = 'localhost';
$db[0]['user'] = 'root';
$db[0]['pass'] = '';
$db[0]['base'] = 'quiz2';

$quizPath = '/var/www/html/quiz2/site/admin/data/quiz/mono/';
$systemPath = '/var/www/html/quiz2/site/admin/data/system/mono/';

$system_file = array(
    'webcome' => $systemPath . 'webcome',
    'main_menu_reg' => $systemPath . 'main_menu_reg',
    'main_menu_cancel' => $systemPath . 'main_menu_cancel',
    'begin_game' => $systemPath . 'begin_game',
    'answer_true' => $systemPath . 'answer_true',
    'answer_false' => $systemPath . 'answer_false',
    'reg_service' => $systemPath . 'reg_service',
    'reg_service_complete' => $systemPath . 'reg_service_complete',
    'cancel_service' => $systemPath . 'cancel_service',
    'cancel_service_complete' => $systemPath . 'cancel_service_complete',
    'point' => $systemPath . 'point',
    'your_score' => $systemPath . 'your_score',
    'high_score' => $systemPath . 'high_score',
    'prize' => $systemPath . 'prize',
    'rule' => $systemPath . 'rule',
    'press' => $systemPath . 'press',
    'begin_service' => $systemPath . 'begin_service',
    'end_service' => $systemPath . 'end_service',
    'win_list' => $systemPath . 'win_list',
    'pin_time_complete' => $systemPath . 'pin_time_complete', 
	'pin_time_successful' => $systemPath . 'pin_time_successful'
);

$logFile = "/var/lib/asterisk/agi-bin/quiz2/log/agi.log";
?>

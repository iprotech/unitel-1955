<?php
$quizPath = '/var/www/html/quiz2/site/admin/data/quiz/mono/';
$systemPath = '/var/www/html/quiz2/site/admin/data/systemNew/mono/';

$system_file = array(
    'webcome' => $systemPath . 'welcome1',
    'main_menu_reg' => $systemPath . 'quiz_reg',
    'main_menu_cancel' => $systemPath . 'quiz_cancel',
    'begin_game' => $systemPath . 'quiz_begin_game',
    'answer_true' => $systemPath . 'answer_true',
    'answer_false' => $systemPath . 'answer_false',
    'reg_service' => $systemPath . 'quiz_reg_service',
    'reg_service_complete' => $systemPath . 'quiz_reg_service_complete',
    'cancel_service' => $systemPath . 'quiz_cancel_service',
    'cancel_service_complete' => $systemPath . 'quiz_cancel_service_complete',
    'point' => $systemPath . 'quiz_point',
    'your_score' => $systemPath . 'quiz_your_score',
    'high_score' => $systemPath . 'quiz_high_score',
    'prize' => $systemPath . 'quiz_prize',
    'rule' => $systemPath . 'quiz_rule',
    'press' => $systemPath . 'quiz_press',
    'begin_service' => $systemPath . 'quiz_begin_service',
    'end_service' => $systemPath . 'quiz_end_service',
    'win_list' => $systemPath . 'win_list',
    'pin_time_complete' => $systemPath . 'quiz_input_time', 
    'pin_time_successful' => $systemPath . 'quiz_input_time_success'
);

$logFile = "/var/lib/asterisk/agi-bin/quiz2/log/agi.log";
?>
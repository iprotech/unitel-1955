#!/usr/bin/php -q
<?php
require('lib/phpagi.php');
require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');
require('lib/config112014.php');
require('lib/config01062014.php');


$agi = new AGI();
$mysql = new Mysql($db);
$msisdn = $agi->get_variable("CALLERID(num)", TRUE);


$count = $mysql->Select('question', 'COUNT(id) AS questionCount', 'status = 1 AND convert_status = 1');
if (!$count || $count[0]['questionCount'] == 0) {
    $mysql->Disconnect();
    $agi->exec_goto('quiz2-main-menu', $exten, 1);
    return;
}

$memberId = $agi->get_variable('memberId', TRUE);

$cache = array();
$where = 'status = 1 AND convert_status = 1';

$agi->stream_file($system_file['begin_game']);

$isRun = TRUE;
$sessionId = 0;
while ($isRun) {
    if (!$sessionId)
        $sessionId = time();
    $whereIn = $where;
    if (count($cache) > 0) {
        $whereIn .= ' AND status=1 AND id NOT IN (' . implode(', ', $cache) . ')';
    }
    $result = $mysql->Select('question', 'id, file_path_question, file_path_choose, answer_key', $whereIn, '', 'RAND()', '1');

    if (!$result) {
        $cache = array();
        continue;
    }
    $question = $result[0];

    $cache[] = $question['id'];

    $keyTrue = $question['answer_key'];
    $i = 0;
    while ($i < 3 && $isRun) {
        $break = FALSE;
	$is_ask = false;
        $agi->stream_file($quizPath . getFileName($question['file_path_question']));
        $input = $agi->get_data($quizPath . getFileName($question['file_path_choose']), $timeout, 1);
        switch ($input['result']) {
            case '0':
                $i = 0;
                continue;
                break;
            case '6':
                $i = 0;
                $break = TRUE;
                break;
            case '*':
                $break = TRUE;
                $isRun = FALSE;
                $mysql->Disconnect();
                $agi->exec_goto('quiz2-menu', $exten, 1);
                break;
            case '':
                break;
            case $keyTrue:
                $mysql->ExecuteQuery('UPDATE member SET score = score + 1, new_score = new_score + 1 WHERE id = ' . $memberId);
                $mysql->Insert('member_score', array('member_id', 'score', 'created_datetime', 'score_type', 'session_id','question_id'), array($memberId, 1, date('Y-m-d H:i:s', time()), 1, "{$sessionId}","{$question['id']}"));
		$agi->stream_file($system_file['answer_true']);
                /* Check Exchange */
                //$arrTest = array("2098254325","309810097","309810035");
                //if(in_array($msisdn,$arrTest)){
                    $TT_Score = $mysql->Select('member', 'new_score', 'msisdn="' . $msisdn . '"','','','1');
                    $Total_Score    = $TT_Score[0];
                   if ($Total_Score['new_score'] == '20') {
                        $exchange = 10;
                        $is_ask = TRUE;
                    } else if ($Total_Score['new_score'] == '30') {
                        $exchange = 20;
                        $is_ask = TRUE;
                    } else if ($Total_Score['new_score'] == '40') {
                        $exchange = 30;
                        $is_ask = TRUE;
                    }else if ($Total_Score['new_score'] >= '60') {
                        $exchange = 50;
                        $sql_exchange = 'INSERT INTO score_exchange(member_id, msisdn, score) 
                                                        VALUES(' . $memberId . ', "' . $msisdn . '", "' . $exchange . '")';
                        $mysql->ExecuteQuery($sql_exchange);

                        $sql_emptyscore = 'UPDATE member SET new_score=0 WHERE msisdn="' . $msisdn . '"';
                        $mysql->ExecuteQuery($sql_emptyscore);

                          


                        $LastID = $mysql->Select('score_exchange', 'id', 'member_id=' . $memberId, '', ' created_datetime DESC', '1');
                        $sql_updateREF = 'UPDATE member_score SET exchange_id = "' . $LastID[0]['id'] . '" WHERE exchange_id=0 && member_id= "' . $memberId.'"';                   
                          $mysql->ExecuteQuery($sql_updateREF);
                          //$agi->stream_file($system_file['exchange_' . $exchange]);
                          //$agi->stream_file($systemPath . 'exchange_30');
                          $agi->stream_file($systemPath . 'exchange_50');
                          $agi->exec_goto('quiz2-menu', $exten, 1);
                          exit();
                        break;
                    }

                    if ($is_ask) {
                        $j = 1;
                        while (1) {
                            if($j++>3) {
                                $agi->stream_file($system_file['more_exchange']);
                                break;
                            }
                            $inputConfirm = $agi->get_data($systemPath . 'exchange_' . $exchange);
                            switch ($inputConfirm['result']) {
                                case 1:
                                    $sql_exchange = 'INSERT INTO score_exchange(member_id, msisdn, score) 
                                                        VALUES(' . $memberId . ', "' . $msisdn . '", "' . $exchange . '")';
                                    $mysql->ExecuteQuery($sql_exchange);

                                    $sql_emptyscore = 'UPDATE member SET new_score=0 WHERE msisdn="' . $msisdn . '"';
                                    $mysql->ExecuteQuery($sql_emptyscore);

                                    $LastID = $mysql->Select('score_exchange', 'id', 'member_id=' . $memberId, '', 'id DESC', '1');
                                    $sql_updateREF = 'UPDATE member_score SET exchange_id = ' . $LastID[0]['id'] . ' WHERE exchange_id=0 && member_id=' . $memberId;
                                    $mysql->ExecuteQuery($sql_updateREF);
                                    $is_confirm = TRUE;
                                        $agi->stream_file($system_file['confirm_exchange']);
                                        $agi->exec_goto('quiz2-menu', $exten, 1);
                                        exit();
                                    break;
                                case 2:
                                    $agi->stream_file($system_file['more_exchange']);
                                    $is_confirm = TRUE;
                                    break;
                            }

                            if($is_confirm)
                                break;
                        }
                    }
                //}
                $break = TRUE;
                break;
            default:
                $agi->stream_file($system_file['answer_false']);
                $break = TRUE;
                $sessionId = 0;
                break;
        }
        if ($break) {
            break;
        }
        $i++;
    }
    if ($i == 3) {
        $isRun = FALSE;
        $mysql->Disconnect();
        $agi->exec_goto('quiz2-menu', $exten, 1);
        return;
    }
}
$mysql->Disconnect();
?>

#!/usr/bin/php -q
<?php
require('lib/phpagi.php');
require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');
require('lib/config112014.php');
require('lib/config01062014.php');

$agi = new AGI();
$mysql = new Mysql($db);
$msisdn = $agi->get_variable('CALLERID(num)', TRUE);

    
$memberId = $agi->get_variable('memberId', TRUE);


$i = 0;
while ($i < 3) {
	$i++;
    $input = $agi->get_data($system_file['reg_service'], $timeout, 1);

    switch ($input['result']) {
        case '1':
            do {
                $seviceId = 0;
                $time = date('Y-m-d H:i:s', time());
                $date = date('Y-m-d', time());

                $service = $mysql->Select('service', 'id', "member_id = {$memberId}");
                if ($service) {
                    $seviceId = $service[0]['id'];
                    $mysql->Update('service', array('status', 'expired_date', 'last_call', 'try_charging'), array('1', $time, $date, '0'), "id = {$seviceId}");
                } else {
                    $msc = $agi->get_variable('msc', TRUE);
                    $mysql->Insert('service', array('member_id', 'time_reg', 'expired_date', 'call_time', 'last_call', 'msc', 'status'), array($memberId, $time, $time, $time, $date, $msc, '1'));
                    $service = $mysql->Select('service', 'id', "member_id = {$memberId}");
                }

                if ($seviceId) {
                    $agi->set_variable('serviceId', $seviceId);
                    $agi->stream_file($system_file['reg_service_complete']);
					$mysql->Disconnect();
                    $agi->exec_goto('quiz2-menu', $exten, 1);
                    return;
                }
            } while ($seviceId == 0);
			$i = 1000;
			break;
        case '*':
			$i = 1000;
			$mysql->Disconnect();
            $agi->exec_goto('quiz2-menu', $exten, 1);
            break;
        default:
            break;
    }
}
$mysql->Disconnect();
?>
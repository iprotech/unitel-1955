#!/usr/bin/php -q
<?php
require('lib/phpagi.php');
require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');
require('lib/config112014.php');
require('lib/config01062014.php');


$agi = new AGI();
$mysql = new Mysql($db);
$msisdn = $agi->get_variable('CALLERID(num)', TRUE);

$isMember = FALSE;
do {
    $member = $mysql->Select('member', 'id', 'msisdn = ' . $msisdn);
    if ($member) {
        $memberId = $member[0]['id'];
        $agi->set_variable('memberId', $memberId);
        $service = $mysql->Select('service', 'id', "member_id = {$memberId} AND status = 1");
        if ($service) {
            $agi->set_variable('serviceId', $service[0]['id']);
        } else {
            $agi->set_variable('serviceId', 0);
        }
        $isMember = TRUE;
    } else {
        $mysql->Insert('member', array('msisdn', 'score'), array($msisdn, 0));
    }
} while (!$isMember);

$agi->stream_file($system_file['webcome']);
$mysql->Disconnect();
$agi->exec_goto('quiz2-menu', $exten, 1);
?>
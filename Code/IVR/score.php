#!/usr/bin/php -q
<?php
require('lib/phpagi.php');
require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');
require('lib/config112014.php');
require('lib/config01062014.php');
 
$agi = new AGI();
$mysql = new Mysql($db);
$msisdn = $agi->get_variable('CALLERID(num)', TRUE);


$memberId = $agi->get_variable('memberId', TRUE);
$member = $mysql->Select('member', 'new_score', 'id = ' . $memberId);
$score = intval($member[0]['new_score']);

$highMember = $mysql->Select('member', 'max(new_score) as maxScore');
$highScore = $highMember[0]['maxScore'];

$i = 0;
while ($i < 3) {
	$i++;
    $agi->stream_file($system_file['your_score']);
    $agi->say_number($score);
    $agi->stream_file($system_file['point']);
    $agi->stream_file($system_file['high_score']);
    $agi->say_number($highScore);
    $agi->stream_file($system_file['point']);
    $input = $agi->get_data($system_file['press'], $timeout, 1);

    switch ($input['result']) {
        case '1':
			$i = 1000;
			$mysql->Disconnect();
            $agi->exec_goto('quiz2-gametest', $exten, 1);
            break;
        case '*':
			$i = 1000;
			$mysql->Disconnect();
            $agi->exec_goto('quiz2-menu', $exten, 1);
            break;
        default:
            break;
    }
}
$mysql->Disconnect();
?>
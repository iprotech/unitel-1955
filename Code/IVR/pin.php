#!/usr/bin/php -q
<?php
require('lib/phpagi.php');
require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');
require('lib/config112014.php');
require('lib/config01062014.php');
$agi = new AGI();
$msisdn = $agi->get_variable('CALLERID(num)', TRUE);



    
$mysql = new Mysql($db);

$serviceId = $agi->get_variable('serviceId', TRUE);

$i = 0;
while ($i < 3) {
	$i++;
    $input = $agi->get_data($system_file['pin_time_complete'], $timeout, 2);
    $key = $input['result'];
    while (isset($key) && strlen($key) > 0) {
        if (is_numeric($key)) {
            if (intval($key) >= 0 && intval($key) < 24) {
                $time = $key . ":00:00";
                $mysql->Update('service', array('call_time'), array($time), 'id = ' . $serviceId);
				$agi->stream_file($system_file['pin_time_successful']);
				$i = 1000;
				$mysql->Disconnect();
                $agi->exec_goto('quiz2-menu', $exten, 1);
                return;
            } else {
                break;
            }
        } else {
            if (strlen($key) > 1) {
                $key = substr($key, 0, 1);
                continue;
            } else {
                break;
            }
        }
    }
}
$mysql->Disconnect();
?>
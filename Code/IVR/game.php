#!/usr/bin/php -q
<?php
require('lib/phpagi.php');
require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');
require('lib/config01062014.php');
$agi = new AGI();
$mysql = new Mysql($db);
$msisdn = $agi->get_variable("CALLERID(num)", TRUE);


$count = $mysql->Select('question', 'COUNT(id) AS questionCount', 'status = 1 AND convert_status = 1');
if (!$count || $count[0]['questionCount'] == 0) {
	$mysql->Disconnect();
    $agi->exec_goto('quiz2-main-menu', $exten, 1);
	return;
}

$memberId = $agi->get_variable('memberId', TRUE);

$cache = array();
$where = 'status = 1 AND convert_status = 1';

$agi->stream_file($system_file['begin_game']);

$isRun = TRUE;
$sessionId = 0;
while ($isRun) {
    if(!$sessionId) $sessionId = time();
    $whereIn = $where;
    if (count($cache) > 0) {
        $whereIn .= ' AND id NOT IN (' . implode(', ', $cache) . ')';
    }
    $result = $mysql->Select('question', 'id, file_path_question, file_path_choose, answer_key', $whereIn, '', 'RAND()', '1');

    if (!$result) {
        $cache = array();
        continue;
    }
    $question = $result[0];

    $cache[] = $question['id'];

    $keyTrue = $question['answer_key'];
    $i = 0;
    while ($i < 3 && $isRun) {
        $break = FALSE;
        $agi->stream_file($quizPath . getFileName($question['file_path_question']));
        $input = $agi->get_data($quizPath . getFileName($question['file_path_choose']), $timeout, 1);
        switch ($input['result']) {
            case '0':
                $i = 0;
                continue;
                break;
            case '6':
		$i = 0;
                $break = TRUE;
                break;
            case '*':
                $break = TRUE;
                $isRun = FALSE;
                $mysql->Disconnect();
                $agi->exec_goto('quiz2-menu', $exten, 1);
                break;
            case '':
                break;
            case $keyTrue:
                $mysql->ExecuteQuery('UPDATE member SET score = score + 1, new_score = new_score + 1 WHERE id = ' . $memberId);
                $mysql->Insert('member_score', array('member_id', 'score', 'created_datetime','score_type','session_id'), array($memberId, 1, date('Y-m-d H:i:s', time()),1,"{$sessionId}"));
                $agi->stream_file($system_file['answer_true']);
                $break = TRUE;
                break;
            default:
                $agi->stream_file($system_file['answer_false']);
                $break = TRUE;
                $sessionId = 0;
                break;
        }
        if ($break){
            break;
        }
	$i++;
    }
    if ($i == 3) {
            $isRun = FALSE;
            $mysql->Disconnect();
            $agi->exec_goto('quiz2-menu', $exten, 1);
            return;
    }
}
$mysql->Disconnect();
?>
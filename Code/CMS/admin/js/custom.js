/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(function(){
    $('#btn-export').live('click', function(){
        var url = $(this).attr('url');
        window.location = url + '?export=true&' + $(this).parents('form').serialize();
        return false;
    });
    
    $(".btn-add-playlist").live('click', function(){
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success:function(data){
                if (data.status) {
                    $(data.html).dialog({
                        title: "Add to...",
                        height: 250,
                        width: 380,
                        modal: true,
                        beforeClose:function(){
                            $(this).remove();
                        },
                        buttons: {
                            "Add":function(){
                                var dialog = this;
                                $(dialog).find('form').ajaxSubmit({
                                    url: url,
                                    type: "POST",
                                    success: function(data) {
                                        $(dialog).dialog("close");
                                    },
                                    error: function(a, b, c) {
                                        
                                    }
                                });
                            },
                            "Cancel":function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            },
            error:function(a, b, c){
                
            }
        });
        return false;
    });
});
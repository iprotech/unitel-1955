<?php

class ChargingHistoryController extends AccessController {

    public $layout = '//layouts/column1';

    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
        $this->moduleId = 10;
        $this->moduleName = Yii::app()->params['module'][$this->moduleId]['name'];
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ChargingHistory('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ChargingHistory']))
            $model->attributes = $_GET['ChargingHistory'];

        if(Yii::app()->request->getParam('export')) {
            $this->exportData($model, array('id', 'member_id', 'msisdn', 'price', 'charging_time'), 'export-charginghistory');
            Yii::app()->end();
        }
        
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = ChargingHistory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'charging-history-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

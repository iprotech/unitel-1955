<?php

class QuestionController extends AccessController {

    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
        $this->moduleId = 1;
        $this->moduleName = Yii::app()->params['module'][$this->moduleId]['name'];
    }

    function getFileName($name) {
        return substr($name, 0, strrpos($name, '.'));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Question;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Question'])) {
            $model->attributes = $_POST['Question'];
            $model->mp3Question = CUploadedFile::getInstance($model, 'mp3Question');
            $model->mp3Choose = CUploadedFile::getInstance($model, 'mp3Choose');
            if ($model->save()) {
                $upload = FALSE;
                if (isset($model->mp3Question) && $model->mp3Question) {
                    $fileName = preg_replace('/\s+/', '', $model->id . '_question_' . $model->mp3Question->getName());
                    $model->mp3Question->saveAs(Yii::app()->params['path']['quiz'] . $fileName);
                    $model->file_path_question = $fileName;
                    $upload = TRUE;
                }
                if (isset($model->mp3Choose) && $model->mp3Choose) {
                    $fileName = preg_replace('/\s+/', '', $model->id . '_choose_' . $model->mp3Choose->getName());
                    $model->mp3Choose->saveAs(Yii::app()->params['path']['quiz'] . $fileName);
                    $model->file_path_choose = $fileName;
                    $upload = TRUE;
                }
                if ($upload) {
                    $model->convert_status = 0;
                    $model->save();
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Question'])) {
            $model->attributes = $_POST['Question'];
            $model->mp3Question = CUploadedFile::getInstance($model, 'mp3Question');
            $model->mp3Choose = CUploadedFile::getInstance($model, 'mp3Choose');
            if ($model->save()) {
                $upload = FALSE;
                if (isset($model->mp3Question) && $model->mp3Question) {
                    if ($model->mp3QuestionOld) {
                        if (file_exists(Yii::app()->params['path']['quiz'] . $model->mp3QuestionOld))
                            unlink(Yii::app()->params['path']['quiz'] . $model->mp3QuestionOld);
                        if (file_exists(Yii::app()->params['path']['quizMono'] . $this->getFileName($model->mp3QuestionOld) . '.alaw'))
                            unlink(Yii::app()->params['path']['quizMono'] . $this->getFileName($model->mp3QuestionOld) . '.alaw');
                    }

                    $fileName = preg_replace('/\s+/', '', $model->id . '_question_' . $model->mp3Question->getName());
                    $model->mp3Question->saveAs(Yii::app()->params['path']['quiz'] . $fileName);
                    $model->file_path_question = $fileName;
                    $upload = TRUE;
                }

                if (isset($model->mp3Choose) && $model->mp3Choose) {
                    if ($model->mp3ChooseOld) {
                        if (file_exists(Yii::app()->params['path']['quiz'] . $model->mp3ChooseOld))
                            unlink(Yii::app()->params['path']['quiz'] . $model->mp3ChooseOld);
                        if (file_exists(Yii::app()->params['path']['quizMono'] . $this->getFileName($model->mp3ChooseOld) . '.alaw'))
                            unlink(Yii::app()->params['path']['quizMono'] . $this->getFileName($model->mp3ChooseOld) . '.alaw');
                    }

                    $fileName = preg_replace('/\s+/', '', $model->id . '_choose_' . $model->mp3Choose->getName());
                    $model->mp3Choose->saveAs(Yii::app()->params['path']['quiz'] . $fileName);
                    $model->file_path_choose = $fileName;
                    $upload = TRUE;
                }

                if ($upload) {
                    $model->convert_status = 0;
                    $model->save();
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $model = $this->loadModel($id);
        if ($model->mp3QuestionOld) {
            if (file_exists(Yii::app()->params['path']['quiz'] . $model->mp3QuestionOld))
                unlink(Yii::app()->params['path']['quiz'] . $model->mp3QuestionOld);
            if (file_exists(Yii::app()->params['path']['quizMono'] . $this->getFileName($model->mp3QuestionOld) . '.alaw'))
                unlink(Yii::app()->params['path']['quizMono'] . $this->getFileName($model->mp3QuestionOld) . '.alaw');
        }
        if ($model->mp3ChooseOld) {
            if (file_exists(Yii::app()->params['path']['quiz'] . $model->mp3ChooseOld))
                unlink(Yii::app()->params['path']['quiz'] . $model->mp3ChooseOld);
            if (file_exists(Yii::app()->params['path']['quizMono'] . $this->getFileName($model->mp3ChooseOld) . '.alaw'))
                unlink(Yii::app()->params['path']['quizMono'] . $this->getFileName($model->mp3ChooseOld) . '.alaw');
        }

        $model->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Question('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Question']))
            $model->attributes = $_GET['Question'];

        if (Yii::app()->request->getParam('export')) {
            $this->exportData($model, array('id', 'name', 'status', 'created_datetime', 'answer_key', 'convert_status'), 'export-question');
            Yii::app()->end();
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Question::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'question-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

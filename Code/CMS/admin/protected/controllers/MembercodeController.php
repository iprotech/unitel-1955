<?php

class MembercodeController extends AccessController {

    public $layout = '//layouts/column1';
    
    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
        $this->moduleId = 5;
        $this->moduleName = Yii::app()->params['module'][$this->moduleId]['name'];
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new MemberCode('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MemberCode']))
            $model->attributes = $_GET['MemberCode'];

        if (Yii::app()->request->getParam('export')) {
            $this->exportData($model, array('id', 'member_id', 'msisdn', 'code', 'month'), 'export-code');
            Yii::app()->end();
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = MemberCode::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}

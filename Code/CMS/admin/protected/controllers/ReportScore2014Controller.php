<?php

class ReportScore2014Controller extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
        $this->moduleId = 13;
        $this->moduleName = Yii::app()->params['module'][$this->moduleId]['name'];
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ReportScore2014('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ReportScore2014']))
            $model->attributes = $_GET['ReportScore2014'];
        
        if(Yii::app()->request->getParam('export')) {
            $this->exportData($model, array('id', 'msisdn', 'score', 'created_datetime'), 'export-reportscore');
            Yii::app()->end();
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ReportScore2014 the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = ReportScore2014::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ReportScore2014 $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'report-score2014-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

<?php

class SystemController extends AccessController {

    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
        $this->moduleId = 0;
        $this->moduleName = Yii::app()->params['module'][$this->moduleId]['name'];
    }

    public function actionUpdate($group) {
        $model = System::model()->findByGroup($group);
        foreach ($model as $item) {
            $file = CUploadedFile::getInstanceByName('system[' . $item->id . ']');
            if (isset($file) && $file) {
                if (file_exists(Yii::app()->params['path']['system'] . $item->file_upload))
                    unlink(Yii::app()->params['path']['system'] . $item->file_upload);
                if (file_exists(Yii::app()->params['path']['systemMono'] . $item->file_name . '.alaw'))
                    unlink(Yii::app()->params['path']['systemMono'] . $item->file_name . '.alaw');
                
                $file->saveAs(Yii::app()->params['path']['system'] . $item->file_upload);
                $item->convert_status = 0;
                $item->save();
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Song::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'song-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

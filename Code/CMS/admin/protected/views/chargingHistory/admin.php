<?php
/* @var $this ChargingHistoryController */
/* @var $model ChargingHistory */

$this->breadcrumbs = array(
    'Charging Histories' => array('admin'),
    'Manage',
);

$this->menu = array(
    //array('label' => 'List ChargingHistory', 'url' => array('index')),
    array('label' => 'Create ChargingHistory', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('charging-history-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Charging Histories</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php $provider = $model->search(); ?>

<div style="background-color: #eee; border: 1px solid #999">
    <p>Total Price: <span><?php echo $model->totalPrice; ?></span></p>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'charging-history-grid',
    'dataProvider' => $provider,
    'ajaxUpdate' => FALSE,
    //'filter' => $model,
    'columns' => array(
        'id',
        'member_id',
        'msisdn',
        'price',
        'charging_time'
    ),
));
?>

<?php
/* @var $this ChargingHistoryController */
/* @var $model ChargingHistory */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
            ));
    ?>

    <div class="row attr-inline">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id'); ?>
    </div><div class="row attr-inline">
        <?php echo $form->label($model, 'price'); ?>
        <?php echo $form->textField($model, 'price'); ?>
    </div>
    <div class="clear"></div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'member_id'); ?>
        <?php echo $form->textField($model, 'member_id'); ?>
    </div>

    <div class="row attr-inline">
        <?php echo $form->label($model, 'msisdn'); ?>
        <?php echo $form->textField($model, 'msisdn'); ?>
    </div>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->label($model, 'charging_time'); ?>
        <span>from</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'time_from',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
        <span>to</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'time_to',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
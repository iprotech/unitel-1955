<?php
/* @var $this MembercodeController */
/* @var $model MemberCode */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
            ));
    ?>

    <div class="row">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id'); ?>
    </div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'member_id'); ?>
        <?php echo $form->textField($model, 'member_id'); ?>
    </div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'msisdn'); ?>
        <?php echo $form->textField($model, 'msisdn'); ?>
    </div>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->label($model, 'code'); ?>
        <?php echo $form->textField($model, 'code', array('size' => 60, 'maxlength' => 256)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'month'); ?>
        <span>from</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'time_from',
            'mode' => 'date',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
        <span>to</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'time_to',
            'mode' => 'date',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
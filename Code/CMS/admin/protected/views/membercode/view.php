<?php
/* @var $this MembercodeController */
/* @var $model MemberCode */

$this->breadcrumbs=array(
	'Member Codes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MemberCode', 'url'=>array('index')),
	array('label'=>'Create MemberCode', 'url'=>array('create')),
	array('label'=>'Update MemberCode', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MemberCode', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MemberCode', 'url'=>array('admin')),
);
?>

<h1>View MemberCode #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'member_id',
		'code',
		'month',
	),
)); ?>

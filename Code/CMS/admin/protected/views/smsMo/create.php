<?php
/* @var $this SmsMoController */
/* @var $model SmsMo */

$this->breadcrumbs=array(
	'Sms Mos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SmsMo', 'url'=>array('index')),
	array('label'=>'Manage SmsMo', 'url'=>array('admin')),
);
?>

<h1>Create SmsMo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
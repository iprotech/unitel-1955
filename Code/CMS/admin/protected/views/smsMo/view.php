<?php
/* @var $this SmsMoController */
/* @var $model SmsMo */

$this->breadcrumbs=array(
	'Sms Mos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SmsMo', 'url'=>array('index')),
	array('label'=>'Create SmsMo', 'url'=>array('create')),
	array('label'=>'Update SmsMo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SmsMo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SmsMo', 'url'=>array('admin')),
);
?>

<h1>View SmsMo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'short_code',
		'msisdn',
		'command_code',
		'content',
		'status',
		'created_datetime',
		'updated_datetime',
	),
)); ?>

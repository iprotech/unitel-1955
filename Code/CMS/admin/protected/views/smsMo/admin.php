<?php
/* @var $this SmsMoController */
/* @var $model SmsMo */

$this->breadcrumbs = array(
    'Sms Mos' => array('admin'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List SmsMo', 'url' => array('index')),
    array('label' => 'Create SmsMo', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sms-mo-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sms Mos</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'sms-mo-grid',
    'dataProvider' => $model->search(),
    //'filter' => $model,
    'columns' => array(
        'id',
        'short_code',
        'msisdn',
        'command_code',
        //'content',
        array(
            'name' => 'status',
            'value' => '$data->status == 0 ? "Pending" : "Proceessed"'
        ),
        'created_datetime',
        'updated_datetime',
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}'
        ),
    ),
));
?>

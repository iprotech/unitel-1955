<?php
/* @var $this SmsMoController */
/* @var $model SmsMo */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
            ));
    ?>

    <div class="row attr-inline">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id'); ?>
    </div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', array(1 => 'Pending', 2 => 'Proceessed'), array('prompt' => 'Select status...')); ?>
    </div>
    <div class="clear"></div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'short_code'); ?>
        <?php echo $form->textField($model, 'short_code'); ?>
    </div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'msisdn'); ?>
        <?php echo $form->textField($model, 'msisdn', array('size' => 16, 'maxlength' => 16)); ?>
    </div>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->label($model, 'command_code'); ?>
        <?php echo $form->textField($model, 'command_code', array('size' => 60, 'maxlength' => 64)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'created_datetime'); ?>
        <span>from</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'created_from',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
        <span>to</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'created_to',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'updated_datetime'); ?>
        <span>from</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'updated_from',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
        <span>to</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'updated_to',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
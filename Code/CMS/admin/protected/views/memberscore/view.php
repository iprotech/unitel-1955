<?php
/* @var $this MemberscoreController */
/* @var $model MemberScore */

$this->breadcrumbs=array(
	'Member Scores'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MemberScore', 'url'=>array('index')),
	array('label'=>'Create MemberScore', 'url'=>array('create')),
	array('label'=>'Update MemberScore', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MemberScore', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MemberScore', 'url'=>array('admin')),
);
?>

<h1>View MemberScore #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'member_id',
		'score',
		'created_datetime',
	),
)); ?>

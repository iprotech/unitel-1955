<?php
/* @var $this QuestionController */
/* @var $model Question */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
            ));
    ?>

    <div class="row attr-inline">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id'); ?>
    </div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'answer_key'); ?>
        <?php echo $form->dropDownList($model, 'answer_key', Yii::app()->params['answer_key'], array('prompt' => 'Select answer key...')); ?>
    </div>
    <div class="clear"></div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', array(1 => 'Enable', 0 => 'Disable'), array('prompt' => 'Select status...')); ?>
    </div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'convert_status'); ?>
        <?php echo $form->dropDownList($model, 'convert_status', array(1 => 'Complete', 0 => 'Pendding'), array('prompt' => 'Select status...')); ?>
    </div>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->label($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 1024)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'created_datetime'); ?>
        <span>from</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'time_from',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
        <span>to</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'time_to',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this QuestionController */
/* @var $model Question */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'question-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
            ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 1024)); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', array(1 => 'Enable', 0 => 'Disable')); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'mp3Question'); ?>
        <?php if (!$model->isNewRecord) {
            ?>
            <div class="">
                <?php echo $model->getPlayerQuestion(); ?>
            </div>
        <?php }
        ?>
        <?php echo $form->fileField($model, 'mp3Question'); ?>
        <?php echo $form->error($model, 'mp3Question'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'mp3Choose'); ?>
        <?php if (!$model->isNewRecord) {
            ?>
            <div class="">
                <?php echo $model->getPlayerChoose(); ?>
            </div>
        <?php }
        ?>
        <?php echo $form->fileField($model, 'mp3Choose'); ?>
        <?php echo $form->error($model, 'mp3Choose'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'created_datetime'); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'created_datetime',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
        <?php echo $form->error($model, 'created_datetime'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'answer_key'); ?>
        <?php echo $form->dropDownList($model, 'answer_key', Yii::app()->params['answer_key']); ?>
        <?php echo $form->error($model, 'answer_key'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
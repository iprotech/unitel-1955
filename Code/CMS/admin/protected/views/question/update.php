<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs = array(
    'Questions' => array('admin'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'Create Question', 'url' => array('create')),
    array('label' => 'View Question', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Manage Question', 'url' => array('admin')),
);
?>

<h1>Update Question #<?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
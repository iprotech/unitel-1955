<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs = array(
    'Questions' => array('admin'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Create Question', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('question-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Questions</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'question-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'id',
        'name',
        array(
            'name' => 'status',
            'value' => '$data->status == 1 ? "Enable" : "Disable"'
        ),
        'created_datetime',
        'answer_key',
        array(
            'header' => 'Question',
            'type' => 'raw',
            'value' => '$data->getPlayerQuestion()'
        ),
        array(
            'header' => 'Choose',
            'type' => 'raw',
            'value' => '$data->getPlayerChoose()'
        ),
        array(
            'name' => 'convert_status',
            'value' => '$data->convert_status == 1 ? "Complete" : "Pendding"'
        ),
        array(
            'header' => 'File Status',
            'type' => 'raw',
            'value' => '$data->checkFile()'
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>

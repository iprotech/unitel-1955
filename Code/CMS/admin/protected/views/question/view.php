<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs = array(
    'Questions' => array('admin'),
    $model->name,
);

$this->menu = array(
    array('label' => 'Create Question', 'url' => array('create')),
    array('label' => 'Update Question', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Question', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Question', 'url' => array('admin')),
);
?>

<h1>View Question #<?php echo $model->name; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
        array(
            'name' => 'status',
            'value' => $model->status == 1 ? 'Enable' : 'Disable'
        ),
        'file_path_question',
        'file_path_choose',
        'created_datetime',
        'answer_key',
        array(
            'name' => 'convert_status',
            'value' => $model->convert_status == 1 ? 'Complete' : 'Pendding'
        )
    ),
));
?>

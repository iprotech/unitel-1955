<?php
$this->breadcrumbs = array(
    'System New' => array('update', 'group' => 1),
    'Update',
);

$this->menu = array(
    array('label' => 'SystemNew', 'url' => array('update', 'group' => 1)),
    array('label' => 'NumberNew', 'url' => array('update', 'group' => 2)),
);
?>

<h1>SystemNew Config</h1>

<div class="form">
    <style type="text/css">
        #system-table {
            width: 100%;
        }

        #system-table tr {
            height: 55px;
        }

        #system-table tr > td {
            padding: 3px 5px;
        }
    </style>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'system-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
    ?>
    <table id="system-table">
        <tbody>
            <?php
            foreach ($model as $item) {
                ?>
                <tr title="<?php echo $item->content_eng; ?>">
                    <td>
                        <?php //echo CHtml::label($item->name, 'systemnew_' . $item->id); ?>
			   <strong><?php echo CHtml::label($item->name, 'systemnew_' . $item->id); ?></strong> <br />
                        <p style="padding-left:8px;"><?php echo $item->content_eng; ?></p>
                    </td>
                    <td>
                        <?php echo CHtml::fileField('systemnew[' . $item->id . ']', NULL, array('id' => 'systemnew_' . $item->id)); ?>
                    </td>
                    <td>
                        <?php echo $item->getPlayer(); ?>
                    </td>
                    <td>
                        <span>
                            <?php echo $item->checkStatus(); ?>
                        </span>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <div class="row buttons" style="margin-top: 25px;">
        <?php echo CHtml::submitButton('Update'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div>
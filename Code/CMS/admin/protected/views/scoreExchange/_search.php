<?php
/* @var $this ScoreExchangeController */
/* @var $model ScoreExchange */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row" style="float: left; width: 25%;">
		<?php echo $form->label($model,'msisdn',array('style' => 'width: 110px;')); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>16,'maxlength'=>16)); ?>
	</div>
    
        <div class="row" style="float: left; width: 75%;">
		<?php echo $form->label($model,'is_handle',array('style' => 'width: 110px;')); ?>
		<?php echo $form->dropDownlist($model,'is_handle',array(''=>'Select...',0=>'by Member',1=>'by System')); ?>
	</div>

	<div class="row" style="float: left; width: 25%;">
		<?php echo $form->label($model,'time_start',array('style' => 'width: 110px;')); ?>
		<?php
                    $this->widget('CJuiDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'time_start',
                        'mode' => 'datetime',
                        'language' => '',
                        'options' => array(
                            'timeFormat' => 'hh:mm:ss',
                            'dateFormat' => 'yy-mm-dd'
                        )
                    ));
                    ?>
	</div>
    
        <div class="row" style="float: left; width: 75%;">
		<?php echo $form->label($model,'time_end',array('style' => 'width: 105px;')); ?>
		<?php
                    $this->widget('CJuiDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'time_end',
                        'mode' => 'datetime',
                        'language' => '',
                        'options' => array(
                            'timeFormat' => 'hh:mm:ss',
                            'dateFormat' => 'yy-mm-dd'
                        )
                    ));
                    ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
                <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this ScoreExchangeController */
/* @var $model ScoreExchange */

$this->breadcrumbs=array(
	'Score Exchanges'=>array('admin'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#score-exchange-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Score Exchanges</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'score-exchange-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		//'member_id',
		'msisdn',
                array('name' => 'score', 'value' => '$data->getExchange()'),
		'created_datetime',
                array('name'=>'is_handle', 'value' => '$data->getHandle()'),
		//'convert',
		array(
			'class'=>'CButtonColumn',
                        'template' => '{view}{delete}'
		),
	),
)); ?>

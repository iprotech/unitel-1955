<?php
/* @var $this ScoreExchangeController */
/* @var $model ScoreExchange */

$this->breadcrumbs=array(
	'Score Exchanges'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ScoreExchange', 'url'=>array('index')),
	array('label'=>'Manage ScoreExchange', 'url'=>array('admin')),
);
?>

<h1>Create ScoreExchange</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
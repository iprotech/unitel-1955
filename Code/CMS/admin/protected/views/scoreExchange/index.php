<?php
/* @var $this ScoreExchangeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Score Exchanges',
);

$this->menu=array(
	array('label'=>'Create ScoreExchange', 'url'=>array('create')),
	array('label'=>'Manage ScoreExchange', 'url'=>array('admin')),
);
?>

<h1>Score Exchanges</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

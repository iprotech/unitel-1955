<?php
/* @var $this ScoreExchangeController */
/* @var $model ScoreExchange */

$this->breadcrumbs=array(
	'Score Exchanges'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ScoreExchange', 'url'=>array('index')),
	array('label'=>'Create ScoreExchange', 'url'=>array('create')),
	array('label'=>'View ScoreExchange', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ScoreExchange', 'url'=>array('admin')),
);
?>

<h1>Update ScoreExchange <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
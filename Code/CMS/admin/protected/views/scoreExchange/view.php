<?php
/* @var $this ScoreExchangeController */
/* @var $model ScoreExchange */

$this->breadcrumbs=array(
	'Score Exchanges'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Delete ScoreExchange', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ScoreExchange', 'url'=>array('admin')),
);
?>

<h1>View ScoreExchange #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'member_id',
		'msisdn',
		'score',
		'created_datetime',
		'convert',
	),
)); ?>

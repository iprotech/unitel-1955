<?php
/* @var $this ScoreExchangeController */
/* @var $data ScoreExchange */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('member_id')); ?>:</b>
	<?php echo CHtml::encode($data->member_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('msisdn')); ?>:</b>
	<?php echo CHtml::encode($data->msisdn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('score')); ?>:</b>
	<?php echo CHtml::encode($data->score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->created_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('convert')); ?>:</b>
	<?php echo CHtml::encode($data->convert); ?>
	<br />


</div>
<?php
/* @var $this ReportingController */
/* @var $model Reporting */

$this->breadcrumbs=array(
	'Reportings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Reporting', 'url'=>array('index')),
	array('label'=>'Manage Reporting', 'url'=>array('admin')),
);
?>

<h1>Create Reporting</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
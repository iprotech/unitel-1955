<?php
/* @var $this ReportingController */
/* @var $model Reporting */

$this->breadcrumbs=array(
	'Reportings'=>array('admin'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'Create Reporting', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reporting-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Reportings</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'reporting-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'time',
		'new_register',
		'cancel_by_user',
		//'cancel_by_system',
		'real_development',
		'current_user',
		'chargable_subs',
		'monthly_fee_revenue',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

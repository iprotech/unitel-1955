<?php
/* @var $this ReportingController */
/* @var $model Reporting */

$this->breadcrumbs=array(
	'Reportings'=>array('admin'),
	$model->id,
);

$this->menu=array(
	//array('label'=>'List Reporting', 'url'=>array('index')),
	//array('label'=>'Create Reporting', 'url'=>array('create')),
	//array('label'=>'Update Reporting', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Reporting', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Reporting', 'url'=>array('admin')),
);
?>

<h1>View Reporting #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'time',
		'new_register',
		'cancel_by_user',
		'cancel_by_system',
		'real_development',
		'current_user',
		'chargable_subs',
		'monthly_fee_revenue',
	),
)); ?>

<?php
/* @var $this ReportingController */
/* @var $model Reporting */

$this->breadcrumbs=array(
	'Reportings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Reporting', 'url'=>array('index')),
	array('label'=>'Create Reporting', 'url'=>array('create')),
	array('label'=>'View Reporting', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Reporting', 'url'=>array('admin')),
);
?>

<h1>Update Reporting <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
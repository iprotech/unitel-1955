<?php
/* @var $this CdrController */
/* @var $model Cdr */

$this->breadcrumbs = array(
    'Cdrs' => array('admin'),
    'Manage'
);

$this->menu = array(
    //array('label' => 'List Cdr', 'url' => array('index')),
    array('label' => 'Create Cdr', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('cdr-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Cdrs</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php $provider = $model->search(); ?>

<div>
    <table style="width: 500px; background-color: #eee; border: 1px solid #eee;">
        <tbody>
            <tr>
                <td rowspan="2" style="text-align: center; font-weight: bold; width: 200px;">Type</td>
                <td colspan="2" style="text-align: center; font-weight: bold; width: 120px;">Total Call</td>
                <td colspan="2" style="text-align: center; font-weight: bold; width: 120px;">Only calls coming</td>
            </tr>
            <tr>
                <td style="text-align: center; font-weight: bold;">Call</td>
                <td style="text-align: center; font-weight: bold;">Block</td>
                <td style="text-align: center; font-weight: bold;">Call</td>
                <td style="text-align: center; font-weight: bold;">Block</td>
            </tr>
            <tr>
                <td>All</td>
                <td><?php echo $model->totalCall["totalAll"]; ?></td>
                <td><?php echo $model->totalBlock["totalAll"]; ?></td>
                <td><?php echo $model->totalCall["total"]; ?></td>
                <td><?php echo $model->totalBlock["total"]; ?></td>
            </tr>
            <tr>
                <td>Duration > 5s AND Free 5s</td>
                <td><?php echo $model->totalCall["free5sAll"]; ?></td>
                <td><?php echo $model->totalBlock["free5sAll"]; ?></td>
                <td><?php echo $model->totalCall["free5s"]; ?></td>
                <td><?php echo $model->totalBlock["free5s"]; ?></td>
            </tr>
        </tbody>
    </table>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'cdr-grid',
    'dataProvider' => $provider,
    'ajaxUpdate' => FALSE,
    //'filter' => $model,
    'columns' => array(
        'id',
        'start',
        //'callerid',
        'src',
        'dst',
        //'dcontext',
        //'channel',
        //'dstchannel',
        //'lastapp',
        //'lastdata',
        'duration',
        'billsec',
        //'disposition',
        //'amaflags',
        //'accountcode',
        //'userfield',
        //'uniqueid',
        //'status',
        //'inform_status',
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}'
        )
    )
));
?>



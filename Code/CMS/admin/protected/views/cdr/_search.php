<?php
/* @var $this CdrController */
/* @var $model Cdr */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
            ));
    ?>

    <div class="row">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'src'); ?>
        <?php echo $form->textField($model, 'src', array('size' => 60, 'maxlength' => 80)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'dst'); ?>
        <?php echo $form->textField($model, 'dst', array('size' => 60, 'maxlength' => 80)); ?>
    </div>
    <div class="clear"></div>
    <!--div class="row attr-inline">
        <?php echo $form->label($model, 'duration'); ?>
        <?php echo $form->textField($model, 'duration'); ?>
    </div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'billsec'); ?>
        <?php echo $form->textField($model, 'billsec'); ?>
    </div>
    <div class="clear"></div-->
    <div class="row">
        <?php echo $form->label($model, 'start'); ?>
        <span>from</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'time_from',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
        <span>to</span>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            'attribute' => 'time_to',
            'mode' => 'datetime',
            'language' => '',
            'options' => array(
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd'
            )
        ));
        ?>
    </div>
    
    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
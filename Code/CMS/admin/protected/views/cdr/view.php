<?php
/* @var $this CdrController */
/* @var $model Cdr */

$this->breadcrumbs = array(
    'Cdrs' => array('admin'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Delete Cdr', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Cdr', 'url' => array('admin')),
);
?>

<h1>View Cdr #<?php echo $model->id; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'start',
        'callerid',
        'src',
        'dst',
        'dcontext',
        'channel',
        'dstchannel',
        'lastapp',
        'lastdata',
        'duration',
        'billsec',
        'disposition',
        'amaflags',
        'accountcode',
        'userfield',
        'uniqueid',
        'status',
        'inform_status',
    ),
));
?>

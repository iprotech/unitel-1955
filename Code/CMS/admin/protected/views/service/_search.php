<?php
/* @var $this ServiceController */
/* @var $model Service */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row attr-inline">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id'); ?>
    </div>
    <div class="row attr-inline">
        <?php echo $form->label($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', array(1 => 'Active', 0 => 'Inactive'), array('prompt' => 'Select status')); ?>
    </div>
    <div class="clear">
        <div class="row attr-inline">
            <?php echo $form->label($model, 'member_id'); ?>
            <?php echo $form->textField($model, 'member_id'); ?>
        </div>
        <div class="row attr-inline">
            <?php echo $form->label($model, 'msisdn'); ?>
            <?php echo $form->textField($model, 'msisdn'); ?>
        </div>
        <div class="clear">
            <div class="row">
                <?php echo $form->label($model, 'time_reg'); ?>
                <span>from</span>
                <?php
                $this->widget('CJuiDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'time_reg_from',
                    'mode' => 'datetime',
                    'language' => '',
                    'options' => array(
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat' => 'yy-mm-dd'
                    )
                ));
                ?>
                <span>to</span>
                <?php
                $this->widget('CJuiDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'time_reg_to',
                    'mode' => 'datetime',
                    'language' => '',
                    'options' => array(
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat' => 'yy-mm-dd'
                    )
                ));
                ?>
            </div>

            <div class="row">
                <?php echo $form->label($model, 'expired_date'); ?>
                <span>from</span>
                <?php
                $this->widget('CJuiDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'expired_date_from',
                    'mode' => 'datetime',
                    'language' => '',
                    'options' => array(
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat' => 'yy-mm-dd'
                    )
                ));
                ?>
                <span>to</span>
                <?php
                $this->widget('CJuiDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'expired_date_to',
                    'mode' => 'datetime',
                    'language' => '',
                    'options' => array(
                        'timeFormat' => 'hh:mm:ss',
                        'dateFormat' => 'yy-mm-dd'
                    )
                ));
                ?>
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton('Search'); ?>
                <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- search-form -->
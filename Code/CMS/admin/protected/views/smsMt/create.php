<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */

$this->breadcrumbs=array(
	'Sms Mts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SmsMt', 'url'=>array('index')),
	array('label'=>'Manage SmsMt', 'url'=>array('admin')),
);
?>

<h1>Create SmsMt</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */

$this->breadcrumbs=array(
	'Sms Mts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SmsMt', 'url'=>array('index')),
	array('label'=>'Create SmsMt', 'url'=>array('create')),
	array('label'=>'Update SmsMt', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SmsMt', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SmsMt', 'url'=>array('admin')),
);
?>

<h1>View SmsMt #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'short_code',
		'msisdn',
		'status',
		'type',
		'content',
		'created_datetime',
		'updated_datetime',
		'phonenumber',
	),
)); ?>

<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */

$this->breadcrumbs = array(
    'Sms Mts' => array('admin'),
    'Manage',
);

$this->menu = array(
    //array('label' => 'List SmsMt', 'url' => array('index')),
    array('label' => 'Create SmsMt', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sms-mt-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sms Mts</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'sms-mt-grid',
    'dataProvider' => $model->search(),
    //'filter'=>$model,
    'columns' => array(
        'id',
        'short_code',
        'msisdn',
        array(
            'name' => 'status',
            'value' => '$data->status == 0 ? "Pending" : "Proceessed"'
        ),
        array(
            'name' => 'type',
            'value' => '$data->type == 1 ? "Text" : "Wappush"'
        ),
        //'content',
        'created_datetime',
        'updated_datetime',
        'phonenumber',
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}'
        ),
    ),
));
?>

<?php
/* @var $this ReportScore2014Controller */
/* @var $model ReportScore2014 */

$this->breadcrumbs=array(
	'Report Score2014s'=>array('admin'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#report-score2014-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Report Score2014s</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'report-score2014-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'msisdn',
		'score',
		'created_datetime',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<?php
/* @var $this ReportScore2014Controller */
/* @var $model ReportScore2014 */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row" style="width:25%; float: left;">
		<?php echo $form->label($model,'msisdn',array('style'=>'width:105px;')); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row" style="width:25%; float: left;">
		<?php echo $form->label($model,'time_start',array('style'=>'width:110px;')); ?>
		<?php
                    $this->widget('CJuiDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'time_start',
                        'mode' => 'datetime',
                        'language' => '',
                        'options' => array(
                            'timeFormat' => 'hh:mm:ss',
                            'dateFormat' => 'yy-mm-dd'
                        )
                    ));
                ?>
	</div>
    
        <div class="row" style="width:50%; float: left;">
		<?php echo $form->label($model,'time_end',array('style'=>'width:105px;')); ?>
		<?php
                    $this->widget('CJuiDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'time_end',
                        'mode' => 'datetime',
                        'language' => '',
                        'options' => array(
                            'timeFormat' => 'hh:mm:ss',
                            'dateFormat' => 'yy-mm-dd'
                        )
                    ));
                ?>
	</div>
    
        <div class="row" style="width:25%; float: left;">
		<?php echo $form->label($model,'score_start',array('style'=>'width:105px;')); ?>
		<?php echo $form->textField($model,'score_start',array('size'=>20,'maxlength'=>20)); ?>
	</div>
    
        <div class="row" style="width:75%; float: left;">
		<?php echo $form->label($model,'score_end',array('style'=>'width:110px;')); ?>
		<?php echo $form->textField($model,'score_end',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
                <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
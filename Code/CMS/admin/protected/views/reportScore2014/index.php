<?php
/* @var $this ReportScore2014Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Report Score2014s',
);

$this->menu=array(
	array('label'=>'Create ReportScore2014', 'url'=>array('create')),
	array('label'=>'Manage ReportScore2014', 'url'=>array('admin')),
);
?>

<h1>Report Score2014s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

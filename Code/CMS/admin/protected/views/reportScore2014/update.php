<?php
/* @var $this ReportScore2014Controller */
/* @var $model ReportScore2014 */

$this->breadcrumbs=array(
	'Report Score2014s'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ReportScore2014', 'url'=>array('index')),
	array('label'=>'Create ReportScore2014', 'url'=>array('create')),
	array('label'=>'View ReportScore2014', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ReportScore2014', 'url'=>array('admin')),
);
?>

<h1>Update ReportScore2014 <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this ReportScore2014Controller */
/* @var $model ReportScore2014 */

$this->breadcrumbs=array(
	'Report Score2014s'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ReportScore2014', 'url'=>array('index')),
	array('label'=>'Create ReportScore2014', 'url'=>array('create')),
	array('label'=>'Update ReportScore2014', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ReportScore2014', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ReportScore2014', 'url'=>array('admin')),
);
?>

<h1>View ReportScore2014 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'msisdn',
		'score',
		'created_datetime',
	),
)); ?>

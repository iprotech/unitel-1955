<?php
/* @var $this ReportScore2014Controller */
/* @var $model ReportScore2014 */

$this->breadcrumbs=array(
	'Report Score2014s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ReportScore2014', 'url'=>array('index')),
	array('label'=>'Manage ReportScore2014', 'url'=>array('admin')),
);
?>

<h1>Create ReportScore2014</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
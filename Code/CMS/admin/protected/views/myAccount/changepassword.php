<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs = array(
    'Accounts' => array('admin'),
    $model->username => array('view', 'id' => $model->id),
    'Change Password',
);
?>

<h1>Change Password Account <?php echo $model->username; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
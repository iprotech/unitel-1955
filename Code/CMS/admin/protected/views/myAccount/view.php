<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs = array(
    'Accounts' => array('admin'),
    $model->username,
);
?>

<h1>View Account #<?php echo $model->username; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'username',
        array(
            'name' => 'password',
            'value' => '******'
        ),
        array(
            'name' => 'module_ids',
            'value' => $model->getModuleListNames()
        ),
        array(
            'name' => 'status',
            'value' => $model->status == 1 ? "Active" : "Inactive"
        ),
    ),
));
?>

<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'member-form',
        'enableAjaxValidation' => false,
            ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'msisdn'); ?>
        <?php echo $form->textField($model, 'msisdn', array('size' => 16, 'maxlength' => 16)); ?>
        <?php echo $form->error($model, 'msisdn'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'score'); ?>
        <?php echo $form->textField($model, 'score'); ?>
        <?php echo $form->error($model, 'score'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
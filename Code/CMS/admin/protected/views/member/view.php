<?php
/* @var $this MemberController */
/* @var $model Member */

$this->breadcrumbs = array(
    'Members' => array('admin'),
    $model->msisdn,
);

$this->menu = array(
    array('label' => 'Create Member', 'url' => array('create')),
    array('label' => 'Update Member', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Member', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Member', 'url' => array('admin')),
);
?>

<h1>View Member #<?php echo $model->msisdn; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'msisdn',
        'score',
    ),
));
?>

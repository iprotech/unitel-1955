<?php
/* @var $this MemberController */
/* @var $model Member */

$this->breadcrumbs = array(
    'Members' => array('admin'),
    $model->msisdn => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'Create Member', 'url' => array('create')),
    array('label' => 'View Member', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Manage Member', 'url' => array('admin')),
);
?>

<h1>Update Member #<?php echo $model->msisdn; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
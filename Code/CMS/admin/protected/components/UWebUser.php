<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UWebUser
 *
 * @author BUI VAN UY
 */
class UWebUser extends CWebUser {

    private $account;

    public function getMainMenu() {
        $menu = array();
        $account = $this->getAccount();
        if ($account) {
            $moduleList = Yii::app()->params['module'];
            $accountModules = explode(',', $account->module_ids);
            foreach ($accountModules as $item) {
                if (isset($moduleList[$item]))
                    $menu[] = array('label' => $moduleList[$item]['name'], 'url' => $moduleList[$item]['url']);
            }
        }
        if ($this->isGuest) {
            $menu[] = array('label' => 'Login', 'url' => array('/site/login'));
        } else {
            $menu[] = array('label' => 'My Password', 'url' => array('/myAccount/changepassword'));
            $menu[] = array('label' => 'Logout', 'url' => array('/site/logout'));
        }
        return $menu;
    }

    public function authenticate($moduleId) {
        $account = $this->getAccount();
        if ($account) {
            $accountModules = explode(',', $account->module_ids);
            //return array_key_exists($moduleId, $accountModules);
            return in_array($moduleId, $accountModules);
        }
        return FALSE;
    }

    public function getAccount() {
        if (!$this->account)
            $this->account = Account::model()->findByAttributes(array('username' => $this->id, 'status' => 1));
        return $this->account;
    }

}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccessController
 *
 * @author BUI VAN UY
 */
class AccessController extends Controller {

    public $layout = '//layouts/column2';
    public $defaultAction = 'admin';
    public $moduleId;
    public $moduleName;

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'create', 'update', 'view', 'export', 'test'),
                'users' => array('@'),
                'expression' => (string) Yii::app()->user->authenticate($this->moduleId)
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionExport() {
        return null;
    }

    public function behaviors() {
        return array(
            'eexcelview' => array(
                'class' => 'application.extensions.eexcelview.EExcelBehavior',
            )
        );
    }

    public function exportData($model, $attributes, $filename) {
        $data = $model->search();
        $data->setPagination(FALSE);
        $attributesList = array();
        foreach ($attributes as $item) {
            $attributesList[] = $item . '::' . $model->getAttributeLabel($item);
        }
        $this->toExcel($data, $attributesList, $filename, array('creator' => Yii::app()->user->name), 'Excel2007');
    }

}

?>

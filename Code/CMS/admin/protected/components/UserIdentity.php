<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     *
     * @var interger
     */
    public $account_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        $this->password = md5($this->password);
        $account = Account::model()->findByAttributes(array('username' => $this->username, 'password' => $this->password, 'status' => 1));

        if ($account) {
            $this->account_id = $account->id;
            $this->errorCode = self::ERROR_NONE;
        } else {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        /*
          if (!isset($users[$this->username]))
          $this->errorCode = self::ERROR_USERNAME_INVALID;
          else if ($users[$this->username] !== $this->password)
          $this->errorCode = self::ERROR_PASSWORD_INVALID;
          else
          $this->errorCode = self::ERROR_NONE;
         * 
         */
        return !$this->errorCode;
    }

}
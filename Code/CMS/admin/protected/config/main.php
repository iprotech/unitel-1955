<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'QUIZ Administrator',
    'defaultController' => 'site',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '123456',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        )
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            //'allowAutoLogin' => true,
            'class' => 'UWebUser'
        ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),
         */
        /*
          'db' => array(
          'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/testdrive.db',
          ),
         * 
         */
        // uncomment the following to use a MySQL database
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=quiz2',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        /*
        'db2' => array(
            'connectionString' => 'mysql:host=localhost;dbname=iprotech_quiz',
            'emulatePrepare' => true,
            'username' => 'iprotech',
            'password' => 'iprotech',
            'charset' => 'utf8',
            'class' => 'CDbConnection'
        ),
         * 
         */
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'defaultUrl' => 'question/admin',
        'pathWeb' => array(
            'quiz' => 'data/quiz/mp3/',
            'system' => 'data/system/mp3/',
            'systemNew' => 'data/systemNew/mp3/',
	     'system112014' => 'data/system112014/mp3/',
	     'system142014' => 'data/system142014/mp3/',
	     'system162014' => 'data/system01062014/mp3/',
        ),
        'path' => array(
            'quiz' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/quiz/mp3/',
            'quizMono' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/quiz/mono/',
            'system' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system/mp3/',
            'systemMono' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system/mono/',
            'systemNew' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/systemNew/mp3/',
            'systemNewMono' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/systemNew/mono/',
	     'system112014' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system112014/mp3/',
            'system112014Mono' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system112014/mono/',
	     'system142014' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system142014/mp3/',
            'system142014Mono' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system142014/mono/',
	     'system162014' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system01062014/mp3/',
            'system162014Mono' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system01062014/mono/',
	     'system162014' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system01062014/mp3/',
            'system162014Mono' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../data/system01062014/mono/',
        ),
        'answer_key' => array(
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
            6 => '6',
            7 => '7',
            8 => '8',
            9 => '9'
        ),
        'audioType' => 'mp3, acc, wma, ac3, aif, aifc, aiff, amr, au, snd, dts, mka, mid, midi, rmi, oga, mpc, ofr, ofs, opus, ra, tak, tta, wav, wv, wax',
        'module' => array(
            1 => array(
                'name' => 'Question',
                'url' => array('/question/admin')
            ),
            2 => array(
                'name' => 'Member',
                'url' => array('/member/admin')
            ),
            3 => array(
                'name' => 'Service',
                'url' => array('/service/admin')
            ),
            10 => array(
                'name' => 'Charging',
                'url' => array('/chargingHistory/admin')
            ),
	     11 => array(
                'name' => 'Reporting',
                'url' => array('/reporting/admin')
            ),
            4 => array(
                'name' => 'Member Score',
                'url' => array('/memberscore/admin')
            ),
            5 => array(
                'name' => 'Score Exchange',
                'url' => array('/scoreExchange/admin')
            ),
            6 => array(
                'name' => 'MO',
                'url' => array('/smsMo/admin')
            ),
            7 => array(
                'name' => 'MT',
                'url' => array('/smsMt/admin')
            ),
            8 => array(
                'name' => 'CDR',
                'url' => array('/cdr/admin')
            ),
            0 => array(
                'name' => 'System',
                'url' => array('/system/update', 'group' => 1)
            ),
            12 => array(
                'name' => 'System New',
                'url' => array('/systemNew/update', 'group' => 1)
            ),
            9 => array(
                'name' => 'Account',
                'url' => array('/account/admin')
            ),
	     13 => array(
                'name' => 'Report Score',
                'url' => array('/reportScore2014/admin', 'group' => 1)
            ),
        )
    ),
);
<?php

/**
 * This is the model class for table "charging_history".
 *
 * The followings are the available columns in table 'charging_history':
 * @property integer $id
 * @property integer $member_id
 * @property integer $price
 * @property string $charging_time
 * @property string $msisdn
 */
class ChargingHistory extends CActiveRecord {

    public $time_from, $time_to;
    public $msisdn;
    
    public $totalPrice;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ChargingHistory the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'charging_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('member_id, price', 'numerical', 'integerOnly' => true),
            array('charging_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, member_id, price, msisdn, time_from, time_to', 'safe', 'on' => 'search'),
            array('member_id, price, charging_time', 'required')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'member_id' => 'Member ID',
            'msisdn' => 'MSISDN',
            'price' => 'Price',
            'charging_time' => 'Charging Time'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = 'h';
        $criteria->join = 'INNER JOIN member AS m ON m.id = h.member_id';

        $criteria->compare('h.id', $this->id);
        $criteria->compare('h.member_id', $this->member_id);
        $criteria->compare('h.price', $this->price);
        $criteria->compare('m.msisdn', $this->msisdn);
        //$criteria->compare('charging_time', $this->charging_time, true);
        if ($this->time_from == date('Y-m-d H:i:s', strtotime($this->time_from))) {
            $criteria->compare('charging_time', '>= ' . $this->time_from, true);
        }
        if ($this->time_to == date('Y-m-d H:i:s', strtotime($this->time_to))) {
            $criteria->compare('charging_time', '<= ' . $this->time_to, true);
        }
        
        $criteria->select = 'sum(price) as totalPrice';
        $statistics = ChargingHistory::model()->find($criteria);
        
        $this->totalPrice = intval($statistics['totalPrice']);
        
        $criteria->select = 'h.id, h.member_id, h.price, h.charging_time, m.msisdn';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 25
                    )
                ));
    }

}
<?php

/**
 * This is the model class for table "sms_mt".
 *
 * The followings are the available columns in table 'sms_mt':
 * @property integer $id
 * @property integer $short_code
 * @property string $msisdn
 * @property integer $status
 * @property integer $type
 * @property string $content
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property string $phonenumber
 */
class SmsMt extends CActiveRecord {

    public $created_from, $created_to, $updated_from, $updated_to;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SmsMt the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sms_mt';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('short_code, status, type', 'numerical', 'integerOnly' => true),
            array('msisdn, phonenumber', 'length', 'max' => 16),
            array('content', 'length', 'max' => 512),
            array('created_datetime, updated_datetime', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, short_code, msisdn, status, type, content, created_from, created_to, updated_from, updated_to, phonenumber', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'short_code' => 'Short Code',
            'msisdn' => 'Msisdn',
            'status' => 'Status',
            'type' => 'Type',
            'content' => 'Content',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'phonenumber' => 'Phonenumber',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('short_code', $this->short_code);
        $criteria->compare('msisdn', $this->msisdn, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('type', $this->type);
        $criteria->compare('content', $this->content, true);
        if ($this->created_from == date('Y-m-d H:i:s', strtotime($this->created_from))) {
            $criteria->compare('created_datetime', '>= ' . $this->created_from, true);
        }
        if ($this->created_to == date('Y-m-d H:i:s', strtotime($this->created_to))) {
            $criteria->compare('created_datetime', '<= ' . $this->created_to, true);
        }
        if ($this->updated_from == date('Y-m-d H:i:s', strtotime($this->updated_from))) {
            $criteria->compare('updated_datetime', '>= ' . $this->updated_from, true);
        }
        if ($this->updated_to == date('Y-m-d H:i:s', strtotime($this->updated_to))) {
            $criteria->compare('updated_datetime', '<= ' . $this->updated_to, true);
        }
        $criteria->compare('phonenumber', $this->phonenumber, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 25
                    )
                ));
    }

}
<?php

/**
 * This is the model class for table "member_code".
 *
 * The followings are the available columns in table 'member_code':
 * @property integer $id
 * @property integer $member_id
 * @property string $code
 * @property string $month
 */
class MemberCode extends CActiveRecord {

    public $msisdn, $time_from, $time_to;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MemberCode the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member_code';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('member_id, code, month', 'required'),
            array('member_id', 'numerical', 'integerOnly' => true),
            array('code', 'length', 'max' => 256),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, member_id, code, month, msisdn, time_from, time_to', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'member_id' => 'Member ID',
            'code' => 'Code',
            'month' => 'Month',
            'msisdn' => 'MSISDN'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->select = 's.id, s.member_id, m.msisdn, s.code, s.month';
        $criteria->alias = 's';
        $criteria->join = 'INNER JOIN member AS m ON m.id = s.member_id';

        $criteria->compare('s.id', $this->id);
        $criteria->compare('s.member_id', $this->member_id);
        $criteria->compare('m.msisdn', $this->msisdn);
        $criteria->compare('s.code', $this->code, true);

        if ($this->time_from == date('Y-m-d H:i:s', strtotime($this->time_from))) {
            $criteria->compare('s.month', '>= ' . $this->time_from, true);
        }
        if ($this->time_to == date('Y-m-d H:i:s', strtotime($this->time_to))) {
            $criteria->compare('s.month', '<= ' . $this->time_to, true);
        }

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 25
                    )
                ));
    }

}
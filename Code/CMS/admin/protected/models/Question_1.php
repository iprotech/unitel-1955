<?php

/**
 * This is the model class for table "question".
 *
 * The followings are the available columns in table 'question':
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property string $file_path_question
 * @property string $file_path_choose
 * @property string $created_datetime
 * @property integer $answer_key
 * @property integer $convert_status
 */
class Question extends CActiveRecord {

    public $time_from, $time_to;
    public $mp3Question, $mp3QuestionOld;
    public $mp3Choose, $mp3ChooseOld;


    public function checkFile() {
        $checkFileQuestion = file_exists(Yii::app()->params['pathWeb']['quiz'] . $this->file_path_question);
        $checkFileChoose = file_exists(Yii::app()->params['pathWeb']['quiz'] . $this->file_path_choose);
        if (!$checkFileQuestion && !$checkFileChoose) {
            return 'File Question & Choose Not Found';
        } else if (!$checkFileQuestion) {
            return 'File Question Not Found';
        } else if (!$checkFileChoose) {
            return 'File Choose Not Found';
        }
        return NULL;
    }

    public function getPlayerQuestion() {
        $path = Yii::app()->params['pathWeb']['quiz'] . $this->file_path_question;
        return $this->player($path);
    }
    
    public function getPlayerChoose() {
        $path = Yii::app()->params['pathWeb']['quiz'] . $this->file_path_choose;
        return $this->player($path);
    }

    private function player($path) {
        return '<object type="application/x-shockwave-flash" data="plugin/dewplayer-mini.swf" width="160" height="20" id="dewplayer" name="dewplayer"><param name="wmode" value="transparent" /><param name="movie" value="plugin/dewplayer-mini.swf" /><param name="flashvars" value="mp3=' . $path . '&amp;showtime=1" /> </object>';
    }

    public function afterFind() {
        $this->mp3QuestionOld = $this->file_path_question;
        $this->mp3ChooseOld = $this->file_path_choose;
        return parent::afterFind();
    }
    
    public function beforeSave() {
        if ($this->created_datetime != date('Y-m-d H:i:s', strtotime($this->created_datetime))) {
            $this->created_datetime = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave();
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Question the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'question';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('status, answer_key, convert_status', 'numerical', 'integerOnly' => true),
            array('name, file_path_question, file_path_choose', 'length', 'max' => 1024),
            array('created_datetime', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, status, created_datetime, answer_key, convert_status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'file_path_question' => 'File Path Question',
            'file_path_choose' => 'File Path Choose',
            'created_datetime' => 'Created Datetime',
            'answer_key' => 'Answer Key',
            'convert_status' => 'Convert Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('answer_key', $this->answer_key);
        $criteria->compare('convert_status', $this->convert_status);

        if ($this->time_from == date('Y-m-d H:i:s', strtotime($this->time_from))) {
            $criteria->compare('created_datetime', '>= ' . $this->time_from, true);
        }
        if ($this->time_to == date('Y-m-d H:i:s', strtotime($this->time_to))) {
            $criteria->compare('created_datetime', '<= ' . $this->time_to, true);
        }

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 25
                    )
                ));
    }

}
<?php

/**
 * This is the model class for table "song".
 *
 * The followings are the available columns in table 'song':
 */
class System extends CActiveRecord {

    public function getPlayer() {
        $path = Yii::app()->params['pathWeb']['system'] . $this->file_upload;
        return $this->player($path);
    }

    private function player($path) {
        return '<object type="application/x-shockwave-flash" data="plugin/dewplayer-mini.swf" width="160" height="20" id="dewplayer" name="dewplayer"><param name="wmode" value="transparent" /><param name="movie" value="plugin/dewplayer-mini.swf" /><param name="flashvars" value="mp3=' . $path . '&amp;showtime=1" /> </object>';
    }

    public function checkStatus() {
        if (!file_exists(Yii::app()->params['pathWeb']['system'] . $this->file_upload)) {
            return 'File Not Found';
        }

        if ($this->convert_status == 1)
            return 'Convert Complete';

        return 'Waiting Convert...';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Song the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'system';
    }

    public function findByGroup($groupKey) {
        return $this->findAllByAttributes(array('group' => $groupKey));
    }

}
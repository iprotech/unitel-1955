<?php

/**
 * This is the model class for table "cdr".
 *
 * The followings are the available columns in table 'cdr':
 * @property integer $id
 * @property string $start
 * @property string $callerid
 * @property string $src
 * @property string $dst
 * @property string $dcontext
 * @property string $channel
 * @property string $dstchannel
 * @property string $lastapp
 * @property string $lastdata
 * @property integer $duration
 * @property integer $billsec
 * @property string $disposition
 * @property integer $amaflags
 * @property string $accountcode
 * @property string $userfield
 * @property string $uniqueid
 * @property integer $status
 * @property integer $inform_status
 */
class Cdr extends CActiveRecord {

    public $time_from, $time_to;
    public $totalCall = array(), $totalBlock = array();

    //private $criteria;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Cdr the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cdr';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('duration, billsec, amaflags, status, inform_status', 'numerical', 'integerOnly' => true),
            array('callerid, src, dst, dcontext, channel, dstchannel, lastapp, lastdata', 'length', 'max' => 80),
            array('disposition', 'length', 'max' => 45),
            array('accountcode', 'length', 'max' => 1024),
            array('userfield, uniqueid', 'length', 'max' => 255),
            array('start', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, start, callerid, src, dst, dcontext, channel, dstchannel, lastapp, lastdata, duration, billsec, disposition, amaflags, accountcode, userfield, uniqueid, status, inform_status, time_from, time_to', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'start' => 'Time',
            'callerid' => 'Callerid',
            'src' => 'Src',
            'dst' => 'Dst',
            'dcontext' => 'Dcontext',
            'channel' => 'Channel',
            'dstchannel' => 'Dstchannel',
            'lastapp' => 'Lastapp',
            'lastdata' => 'Lastdata',
            'duration' => 'Duration',
            'billsec' => 'Billsec',
            'disposition' => 'Disposition',
            'amaflags' => 'Amaflags',
            'accountcode' => 'Accountcode',
            'userfield' => 'Userfield',
            'uniqueid' => 'Uniqueid',
            'status' => 'Status',
            'inform_status' => 'Inform Status',
        );
    }
    
    private function getStatisticsAll($free5s = FALSE) {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('callerid', $this->callerid, true);
        $criteria->compare('src', $this->src);
        $criteria->compare('dst', $this->dst);
        if ($this->time_from == date('Y-m-d H:i:s', strtotime($this->time_from))) {
            $criteria->compare('start', '>= ' . $this->time_from, true);
        }
        if ($this->time_to == date('Y-m-d H:i:s', strtotime($this->time_to))) {
            $criteria->compare('start', '<= ' . $this->time_to, true);
        }
        
        if ($free5s) {
            $criteria->compare('duration', '> 5');
            $criteria->select = 'count(id) as totalCall, sum(ceil((duration - 5)/60)) as totalBlock';
        } else {
            $criteria->select = 'count(id) as totalCall, sum(ceil(duration/60)) as totalBlock';
        }
        $criteria->select = 'count(id) as totalCall, sum(ceil(duration/60)) as totalBlock';
        $statistics = Cdr::model()->find($criteria);
        return $statistics;
    }
    
    private function getStatisticsOnlyComing($free5s = FALSE) {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('callerid', $this->callerid, true);
        $criteria->compare('src', $this->src);
        if ($this->time_from == date('Y-m-d H:i:s', strtotime($this->time_from))) {
            $criteria->compare('start', '>= ' . $this->time_from, true);
        }
        if ($this->time_to == date('Y-m-d H:i:s', strtotime($this->time_to))) {
            $criteria->compare('start', '<= ' . $this->time_to, true);
        }
        
        if ($this->dst)
            $criteria->compare('dst', $this->dst);
        else
            $criteria->compare('dst', '1955');
        
        if ($free5s) {
            $criteria->compare('duration', '> 5');
            $criteria->select = 'count(id) as totalCall, sum(ceil((duration - 5)/60)) as totalBlock';
        } else {
            $criteria->select = 'count(id) as totalCall, sum(ceil(duration/60)) as totalBlock';
        }
        $statistics = Cdr::model()->find($criteria);
        return $statistics;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        /*
         * All
         */
        $statistics1 = $this->getStatisticsAll();
        $this->totalCall["totalAll"] = intval($statistics1['totalCall']);
        $this->totalBlock["totalAll"] = intval($statistics1['totalBlock']);

        /*
         * Only coming 
         */
        $statistics2 = $this->getStatisticsOnlyComing();
        $this->totalCall["total"] = intval($statistics2['totalCall']);
        $this->totalBlock["total"] = intval($statistics2['totalBlock']);

        /*
         * Free 5s all
         */
        $statistics3 = $this->getStatisticsAll(TRUE);
        $this->totalCall["free5sAll"] = intval($statistics3['totalCall']);
        $this->totalBlock["free5sAll"] = intval($statistics3['totalBlock']);

        /*
         * Free 5s Only coming
         */
        $statistics4 = $this->getStatisticsOnlyComing(TRUE);
        $this->totalCall["free5s"] = intval($statistics4['totalCall']);
        $this->totalBlock["free5s"] = intval($statistics4['totalBlock']);
        /*
         * CDR Table
         */
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('callerid', $this->callerid, true);
        $criteria->compare('src', $this->src);
        $criteria->compare('dst', $this->dst);
        if ($this->time_from == date('Y-m-d H:i:s', strtotime($this->time_from))) {
            $criteria->compare('start', '>= ' . $this->time_from, true);
        }
        if ($this->time_to == date('Y-m-d H:i:s', strtotime($this->time_to))) {
            $criteria->compare('start', '<= ' . $this->time_to, true);
        }
	 $criteria->order = 'id DESC';
        $criteria->select = 'id, start, callerid, src, duration, billsec, dst';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 25
            )
        ));
    }

}
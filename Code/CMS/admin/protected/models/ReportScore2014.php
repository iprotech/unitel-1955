<?php

/**
 * This is the model class for table "report_score2014".
 *
 * The followings are the available columns in table 'report_score2014':
 * @property integer $id
 * @property string $msisdn
 * @property integer $score
 * @property string $created_datetime
 */
class ReportScore2014 extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public $time_start, $time_end,$score_start,$score_end;
    public function tableName() {
        return 'report_score2014';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('msisdn, score, created_datetime', 'required'),
            array('score', 'numerical', 'integerOnly' => true),
            array('msisdn', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, msisdn, score, created_datetime, time_start, time_end, score_start, score_end', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'score' => 'Score',
            'created_datetime' => 'Created Datetime',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('msisdn', $this->msisdn, true);
        //$criteria->compare('score', $this->score);
        //$criteria->compare('created_datetime', $this->created_datetime, true);
        if($this->time_start)
            $criteria->compare ('created_datetime', '>='.$this->time_start);
        if($this->time_end)
            $criteria->compare ('created_datetime', '<'.$this->time_end);
        
        if($this->score_start)
            $criteria->compare ('score', '>='.$this->score_start);
        if($this->score_end)
            $criteria->compare ('score', '<'.$this->score_end);
        $criteria->order    = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize'=>25)
                ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ReportScore2014 the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

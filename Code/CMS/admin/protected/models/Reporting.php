<?php

/**
 * This is the model class for table "reporting".
 *
 * The followings are the available columns in table 'reporting':
 * @property integer $id
 * @property string $time
 * @property integer $new_register
 * @property integer $cancel_by_user
 * @property integer $cancel_by_system
 * @property integer $real_development
 * @property integer $current_user
 * @property integer $chargable_subs
 * @property integer $monthly_fee_revenue
 */
class Reporting extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Reporting the static model class
	 */
    
        public $time_start;
        public $time_end;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reporting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('time, new_register, cancel_by_user, cancel_by_system, real_development, current_user, chargable_subs, monthly_fee_revenue', 'required'),
			array('new_register, cancel_by_user, cancel_by_system, real_development, current_user, chargable_subs, monthly_fee_revenue', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, time, time_start, time_end', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'time' => 'Time',
			'new_register' => 'New Register',
			'cancel_by_user' => 'Cancel By User',
			'cancel_by_system' => 'Cancel By System',
			'real_development' => 'Real Development',
			'current_user' => 'Current User',
			'chargable_subs' => 'Chargable Subs',
			'monthly_fee_revenue' => 'Fee Revenue',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		//$criteria->compare('time',$this->time,true);
		$criteria->compare('new_register',$this->new_register);
		$criteria->compare('cancel_by_user',$this->cancel_by_user);
		$criteria->compare('cancel_by_system',$this->cancel_by_system);
		$criteria->compare('real_development',$this->real_development);
		$criteria->compare('current_user',$this->current_user);
		$criteria->compare('chargable_subs',$this->chargable_subs);
		$criteria->compare('monthly_fee_revenue',$this->monthly_fee_revenue);
                if($this->time_start)
                    $criteria->compare ('time', '>='.$this->time_start);
                if($this->time_end)
                    $criteria->compare ('time', '<'.$this->time_end);
                $criteria->order = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination' => array('pageSize' => 25)
		));
	}
}
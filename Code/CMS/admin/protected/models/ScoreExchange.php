<?php

/**
 * This is the model class for table "score_exchange".
 *
 * The followings are the available columns in table 'score_exchange':
 * @property integer $id
 * @property integer $member_id
 * @property string $msisdn
 * @property integer $score
 * @property string $created_datetime
 * @property integer $convert
 */
class ScoreExchange extends CActiveRecord {

    public $time_start, $time_end;
    /**
     * @return string the associated database table name
     * 
     * 
     */
    
    public function getHandle() {
        switch ($this->is_handle) {
            case 1:
                return 'by System';
                break;
            case 0:
                return 'by Member';
                break;
            default:
                return '';
                break;
        }
    }
    
    public function getExchange() {
        switch ($this->score) {
            case 10:
                return number_format(10000).' Kips';
                break;
            case 20:
                return number_format(20000).' Kips';
			case 30:
                return number_format(30000).' Kips';
                break;
            case 50:
                return number_format(50000).' Kips';
                break;
            default:
                return '';
                break;
        }
    }
    
    public function tableName() {
        return 'score_exchange';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('member_id, msisdn, score, created_datetime, convert', 'required'),
            array('member_id, score, convert', 'numerical', 'integerOnly' => true),
            array('msisdn', 'length', 'max' => 16),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, is_handle, member_id, time_start, time_end, msisdn, score, created_datetime, convert', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'member_id' => 'Member',
            'msisdn' => 'Msisdn',
            'score' => 'Score',
            'created_datetime' => 'Created Datetime',
            'convert' => 'Convert',
            'is_handle' => 'Exchange By'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('member_id', $this->member_id);
        $criteria->compare('msisdn', $this->msisdn, true);
        $criteria->compare('created_datetime', $this->created_datetime, true);
        $criteria->compare('is_handle', $this->is_handle);
        if($this->time_start)
            $criteria->compare ('created_datetime', '>'.$this->time_start);
        if($this->time_end)
            $criteria->compare ('created_datetime', '<'.$this->time_end);
        $criteria->order    = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => 25)
                ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ScoreExchange the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

<?php

/**
 * This is the model class for table "service".
 *
 * The followings are the available columns in table 'service':
 * @property integer $id
 * @property integer $member_id
 * @property string $time_reg
 * @property string $expired_date
 * @property datetime $call_time
 * @property string $msisdn
 * @property datetime $time_reg_from
 * @property datetime $time_reg_to
 * @property datetime $expired_date_from
 * @property datetime $expired_date_to
 * @property int $status
 */
class Service extends CActiveRecord {

    public $msisdn;
    public $time_reg_from, $time_reg_to;
    public $expired_date_from, $expired_date_to;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Service the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'service';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('member_id, time_reg, expired_date', 'required'),
            array('member_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, member_id, msisdn, time_reg_from, time_reg_to, expired_date_from, expired_date_to, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'member_id' => 'Member ID',
            'msisdn' => 'MSISDN',
            'time_reg' => 'Time Reg',
            'call_time' => 'Call Time',
            'expired_date' => 'Expired Date',
            'status' => 'Status'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = 's';
        $criteria->join = 'inner join member as m on m.id = s.member_id';
        $criteria->select = 's.id, s.member_id, s.time_reg, s.expired_date, s.call_time, m.msisdn, s.status';

        $criteria->compare('s.id', $this->id);
        $criteria->compare('s.member_id', $this->member_id);
        $criteria->compare('m.msisdn', $this->msisdn);
        $criteria->compare('s.status', $this->status);
        
        if ($this->time_reg_from == date('Y-m-d H:i:s', strtotime($this->time_reg_from))) {
            $criteria->compare('s.time_reg', '>= ' . $this->time_reg_from, true);
        }
        if ($this->time_reg_to == date('Y-m-d H:i:s', strtotime($this->time_reg_to))) {
            $criteria->compare('s.time_reg', '<= ' . $this->time_reg_to, true);
        }
        
        if ($this->expired_date_from == date('Y-m-d H:i:s', strtotime($this->expired_date_from))) {
            $criteria->compare('s.expired_date', '>= ' . $this->expired_date_from, true);
        }
        if ($this->expired_date_to == date('Y-m-d H:i:s', strtotime($this->expired_date_to))) {
            $criteria->compare('s.expired_date', '<= ' . $this->expired_date_to, true);
        }
	$criteria->order = 'id DESC';
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 25
            )
        ));
    }

}
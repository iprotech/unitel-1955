<?php
error_reporting(E_ALL);
require('lib/mysql.lib.php');
require('lib/config.php');
require('lib/functions.php');

$mysql = new Mysql($db);
$time = date('Y-m-d H:i:s', time());

$service = $mysql->ExecuteQuery("SELECT s.id, s.member_id, s.try_charging, s.expired_date, m.msisdn FROM service AS s INNER JOIN member AS m ON s.member_id = m.id
WHERE TIMEDIFF(s.expired_date,'{$time}') < 0 AND s.status = 1 AND s.try_charging < 120
ORDER BY s.try_charging ASC LIMIT 10");

if (isset($service) && $service) {
    foreach ($service as $item) {
        $result = charge($item['msisdn'], $week_fee, $shortcode, $feeType);
        var_dump($result);
        if (!$result[0]) {
            $mysql->ExecuteQuery("UPDATE service SET try_charging = 0, expired_date = DATE_ADD('{$time}',INTERVAL 7 DAY) WHERE id = {$item['id']}");
            $mysql->Insert('charging_history', array('member_id', 'charging_time', 'price'), array($item['member_id'], $time, $week_fee));
            echo 'Charging Complete';
        } else {
            //$mysql->ExecuteQuery("UPDATE service SET status = 0 WHERE (DATEDIFF('{$time}', expired_date) > {$max_try_charging}) AND try_charging > {$max_try_charging}");
            $mysql->ExecuteQuery('UPDATE service SET try_charging = try_charging + 1 WHERE id = ' . $item['id']);
        }
        usleep(100000);
    }
} else {
    $mysql->ExecuteQuery("UPDATE service AS s SET s.try_charging = 0 WHERE s.status = 1 AND s.try_charging >= 120");
    echo 'No Charging';
}
?>
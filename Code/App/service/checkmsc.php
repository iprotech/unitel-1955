<?php

require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');

$mysql = new Mysql($db);

$service = $mysql->ExecuteQuery("SELECT s.id, m.msisdn FROM service AS s INNER JOIN member AS m ON s.member_id = m.id WHERE LENGTH(s.msc) = 0");

if (isset($service) && $service) {
    foreach ($service as $item) {
        $cdr = $mysql->ExecuteQuery("SELECT channel FROM cdr WHERE src = '{$item['msisdn']}' LIMIT 1");
        if (isset($cdr) && $cdr) {
            $msc = FALSE;
            if (strpos($cdr[0]['channel'], "10.20.5.17") !== false) {
                $msc = "msc4";
            } else if (strpos($cdr[0]['channel'], "10.20.3.17") !== false) {
                $msc = "msc2";
            } else if (strpos($cdr[0]['channel'], "10.20.5.1") !== false) {
                $msc = "msc3";
            } else  if (strpos($cdr[0]['channel'], "10.20.3.1") !== false) {
                $msc = "msc1";
            }
            
            if ($msc) {
                $mysql->Update('service', array('msc'), array($msc), "id = {$item['id']}");
            } else {
				$mysql->Update('service', array('msc', 'is_import_msc'), array($msc, "1"), "id = {$item['id']}");
			}
        }
    }
}
?>

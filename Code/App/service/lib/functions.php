<?php

class CallInfo {

    public $msisdn;
    public $maxRetries;
    public $retryTime;
    public $waitTime;
    public $context;
    public $extension;
    public $priority;
    public $callerId;
    public $set;

}

/**
 * 
 * @param CallInfo $callInfo
 * @param string $filePath
 */
function createCallFile($callInfo, $filePath) {
    $content = "Channel: SIP/{$callInfo->msisdn}" .
            "\nMaxRetries: {$callInfo->maxRetries}" .
            "\nRetryTime: {$callInfo->retryTime}" .
            "\nWaitTime: {$callInfo->waitTime}" .
            "\nContext: {$callInfo->context}" .
            "\nExtension: {$callInfo->extension}" .
            "\nPriority: {$callInfo->priority}" .
            "\nCallerID: {$callInfo->callerId}" .
            "\nSet: {$callInfo->set}" .
            "\nAlwaysDelete: Yes";

    if (file_exists("{$filePath}.call"))
        unlink("{$filePath}.call");

    file_put_contents("{$filePath}.call", $content);
}



function viewInfo($msisdn){
    global $_vasgateway;
    $postVar = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.bccsgw.viettel.com/">
                    <soapenv:Header/>
                    <soapenv:Body>
                       <web:gwOperation>
                          <Input>
                            <username>'.$_vasgateway['username'].'</username>
                            <password>'.$_vasgateway['password'].'</password>
                            <wscode>viewinfonew</wscode>
                            <!--1 or more repetitions:-->
                            <param name="GWORDER" value="1"/>
                            <param name="msisdn" value="'.$msisdn.'"/>
                          </Input>
                       </web:gwOperation>
                    </soapenv:Body>
                 </soapenv:Envelope>
               ';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
            CURLOPT_URL	       => $_vasgateway['address'],
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $postVar,
            CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array( $ch, $options );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    $responeContent = curl_exec($ch);
    //var_dump($responeContent);
    $result = array();
    if($err || !$responeContent)
    {
        $result[0] = 201;
    }
    else
    {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>')+8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));
        $result = explode('|', $responeContent);
    }
    return $result;
}


function charge($msisdn,$money,$shortcode,$feeType,$subType=1){
    global $_vasgateway;
    $postVar = '
                    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.bccsgw.viettel.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <web:gwOperation>
                              <Input>
                                <username>'.$_vasgateway['username'].'</username>
                                <password>'.$_vasgateway['password'].'</password>
                                <wscode>chargenew</wscode>
                                <!--1 or more repetitions:-->
                                <param name="GWORDER" value="1"/>
                                <param name="input" value="'.$msisdn.'|'.$shortcode.'|'.$feeType.'|'.$subType.'|'.$money.'|0"/>
                              </Input>
                           </web:gwOperation>
                        </soapenv:Body>
                     </soapenv:Envelope>
               ';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
            CURLOPT_URL	       => $_vasgateway['address'],
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $postVar,
            CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array( $ch, $options );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    $responeContent = curl_exec($ch);
    var_dump($responeContent);
    $result = 0;
    if($err || !$responeContent)
    {
        $result[0] = 201;
    }
    else
    {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>')+8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));
        $result = explode('|',$responeContent);
    }
    return $result;
}




function checkBalance($msisdn, $type = 0) {
    $result = null;
    $data = array();
    try {
        $client = new SoapClient('http://10.78.6.226/ZSmartService/AccountService.asmx?wsdl');
        $params->MSISDN = $msisdn;
        $params->AccountCode = "";
        $result = $client->QueryAcctBal($params);
        if (gettype($result->QueryAcctBalResult->BalDto) == 'array') {
            $data = array(
                'AcctResID' => $result->QueryAcctBalResult->BalDto[$type]->AcctResID,
                'AcctResName' => $result->QueryAcctBalResult->BalDto[$type]->AcctResName,
                'Balance' => $result->QueryAcctBalResult->BalDto[$type]->Balance,
                'BalType' => $result->QueryAcctBalResult->BalDto[$type]->BalType,
                'BalID' => $result->QueryAcctBalResult->BalDto[$type]->BalID,
            );
        }
    } catch (Exception $e) {
        return $data["exception"] = $e->getMessage();
    }
    return $data;
}

function chargingUsingFee($shortCode = "") {
    global $remainSyx, $realPath, $server, $user, $password, $db, $serviceShortCodePrice;
    $conn = null;
    try {
        $conn = new sbMssqlADO($server, $user, $password, $db);
    } catch (Exception $e) {
        file_put_contents($realPath . "log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
        sleep(5);
    }
    if (!$shortCode) {
        $shortCode = "service.shortcode";
    } else {
        $shortCode = "'{$shortCode}'";
    }
    $sqlSelectService = " SELECT * FROM service ORDER BY id ASC";
    $services = $conn->doSelect($sqlSelectService);
    var_dump($services);
    foreach ($services as $service) {
        $dateExpired = date("Ymd", time());
        $sendTime = date("YmdHis", time());
        $sqlUpdateTotalTry = "
                                    UPDATE member SET total_try_charging = total_try_charging+1
                                    WHERE (status=1 OR status=2)
                                              AND service_id={$service['id']}
                                              AND convert(nvarchar(8),end_charging_date,111)<'{$dateExpired}'
                             ";
        echo $sqlUpdateTotalTry;
        $conn->doUpdate($sqlUpdateTotalTry);
        $sqlInsertRemain = "
                                INSERT INTO SMS_REMAIN(UserID,ReceiverID,ServiceID,CommandCode,ContentType,Info,SentTime,Status,SendType)
                                SELECT member.msisdn,{$shortCode},member.msisdn,'Free','0','{$remainSyx} {$service['id']}','{$sendTime}',0,1
                                FROM member INNER JOIN service ON member.service_id=service.id
                                WHERE (status=1 OR status=2)
                                          AND convert(nvarchar(8),end_charging_date,111)<'{$dateExpired}'
                                          AND service_id={$service['id']}
                           ";
        echo $sqlInsertRemain;
        $conn->doUpdate($sqlInsertRemain);

        while (1) {
            $sqlCheck = " SELECT COUNT(*) as total FROM SMS_REMAIN WHERE status=0";
            $checkResult = $conn->doSelectOne($sqlCheck);
            if ($checkResult['total']) {
                $sqlCheckProcess = " SELECT count(*) as total FROM SMS_SENT WHERE status=0 AND SendType =1 ";
                $totalItemRemain = $conn->doSelectOne($sqlCheckProcess);
                if ($totalItemRemain['total'] == 0) {
                    $sqlMin = " SELECT min(ID) as min_id FROM SMS_REMAIN  WHERE Status=0 ";
                    $min = $conn->doSelectOne($sqlMin);
                    $minId = $min['min_id'];
                    if (!$minId)
                        $minId = 0;

                    $sqlMax = "
                                    SELECT max(ID) as max_id FROM SMS_REMAIN
                                    WHERE ID in (SELECT top 10 ID FROM  SMS_REMAIN WHERE Status=0 ORDER BY ID ASC )
                              ";
                    $max = $conn->doSelectOne($sqlMax);
                    $maxId = $max['max_id'];
                    if (!$maxId)
                        $maxId = 0;

                    $sqlSendSms = "
                                        INSERT INTO SMS_SENT(UserID,ReceiverID,ServiceID,CommandCode,ContentType,Info,SentTime,Status,SendType)
                                        SELECT TOP 10 UserID,ReceiverID,ServiceID,CommandCode,ContentType,Info,SentTime,Status,SendType
                                        FROM SMS_REMAIN
                                        WHERE Status=0
                                        ORDER BY ID ASC
                                  ";
                    echo $sqlSendSms;
                    $conn->doUpdate($sqlSendSms);

                    $sqlUpdateRemainStatus = " UPDATE SMS_REMAIN SET STATUS = 1 WHERE ID>={$minId} and ID<={$maxId}";
                    $conn->doUpdate($sqlUpdateRemainStatus);
                    echo $sqlUpdateRemainStatus . "\n";
                    sleep(10);
                }else {
                    echo "\n Sleep 2 seconds to wait send SMS process ";
                    sleep(2);
                }
            } else {
                break;
            }
            sleep(2);
        }
    }

    while (1) {
        $conn = new sbMssqlADO($server, $user, $password, $db);
        $sqlCheckProcess = " SELECT count(*) as total FROM SMS_SENT WHERE status=0 AND SendType =1 ";
        $totalItemRemain = $conn->doSelectOne($sqlCheckProcess);
        if ($totalItemRemain['total'] > 0) {
            sleep(60);
        } else {
            break;
        }
    }
}

function sendSmsRemain($shortCode = "") {
    global $remainSyx, $realPath, $server, $user, $password, $db, $serviceShortCodePrice;
    $conn = null;
    try {
        $conn = new sbMssqlADO($server, $user, $password, $db);
    } catch (Exception $e) {
        file_put_contents($realPath . "log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
        sleep(5);
    }
    if (!$shortCode) {
        $shortCode = "service.shortcode";
    } else {
        $shortCode = "'{$shortCode}'";
    }
    $sqlSelectService = " SELECT * FROM service";
    $services = $conn->doSelect($sqlSelectService);
    var_dump($services);
    foreach ($services as $service) {
        $dateExpired = date("Ymd", time());
        $sendTime = date("YmdHis", time());
        $sqlUpdateTotalTry = "
                                    UPDATE member SET total_try_charging = total_try_charging+1
                                    WHERE (status=1 OR status=2)
                                              AND service_id={$service['id']}
                                              AND convert(nvarchar(8),end_charging_date,111)<'{$dateExpired}'
                             ";
        echo $sqlUpdateTotalTry;
        $conn->doUpdate($sqlUpdateTotalTry);

        $sqlSendSms = "
                            INSERT INTO SMS_SENT(UserID,ReceiverID,ServiceID,CommandCode,ContentType,Info,SentTime,Status,SendType)
                            SELECT member.msisdn,{$shortCode},member.msisdn,'Free','0','{$remainSyx} {$service['id']}','{$sendTime}',0,1
                            FROM member INNER JOIN service ON member.service_id=service.id
                            WHERE (status=1 OR status=2)
                                      AND convert(nvarchar(8),end_charging_date,111)<'{$dateExpired}'
                                      AND service_id={$service['id']}
                       ";
        echo $sqlSendSms;
        $conn->doUpdate($sqlSendSms);
    }

    while (1) {
        $conn = new sbMssqlADO($server, $user, $password, $db);
        $sqlCheckProcess = " SELECT count(*) as total FROM SMS_SENT WHERE status=0 AND SendType =1 ";
        $totalItemRemain = $conn->doSelectOne($sqlCheckProcess);
        if ($totalItemRemain['total'] > 0) {
            sleep(60);
        } else {
            break;
        }
    }
}

function remainProcess() {
    global $remainSyx, $realPath, $server, $user, $password, $db, $serviceShortCodePrice, $totalProcessRecord;
    $sqlSelect = " SELECT TOP {$totalProcessRecord} * FROM SMS_RECV WHERE Status=0 AND CommandCode='{$remainSyx}' ";
    $conn = null;
    while (1) {
        if (!$conn) {
            try {
                $conn = new sbMssqlADO($server, $user, $password, $db);
            } catch (Exception $e) {
                file_put_contents($realPath . "log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
                sleep(5);
            }
        } else {
            $datas = $conn->doSelect($sqlSelect);
            if (count($datas)) {
                foreach ($datas as $data) {
                    $totalDayBonus = 0;
                    $infors = explode(" ", $data['Info']);
                    $serviceId = $infors[1];
                    if ($serviceId) {
                        $sqlSelectService = " SELECT * FROM service WHERE id = '$serviceId' ";
                        $service = $conn->doSelectOne($sqlSelectService);
                        if ($service) {
                            //REMAIN
                            //ON command
                            var_dump($serviceShortCodePrice[$data['ServiceID']]);
                            $shortcodePrice = $serviceShortCodePrice[$data['ServiceID']];
                            $servicePrice = $service['price'];
                            echo "\n Price: " . $shortcodePrice;
                            if ($service['type'] == 1) {
                                //Daily
                                $totalDayBonus = ceil($shortcodePrice / $servicePrice);
                            } else {
                                //Monthy
                                $totalDayBonus = ceil(($shortcodePrice / $servicePrice) * 30);
                            }
                            echo "\n" . $totalDayBonus;
                            $sqlCheckMemberExists = "
                                                        SELECT * FROM member
                                                        WHERE msisdn='{$data['UserID']}' AND service_id='{$service['id']}' AND (status=1 OR status=2)
                                                   ";
                            $member = $conn->doSelectOne($sqlCheckMemberExists);
                            var_dump($member);
                            if ($member) {
                                //Member have been registed already, bonous using days for this member
                                echo "\n Bonous using days for this member";
                                $startChargingDate = date("YmdHis", time());
                                if ($member['status'] == 1) {
                                    //Member is active
                                    $endChargingDate = strtotime($member['end_charging_date']) + 24 * 3600 * $totalDayBonus;
                                } else {
                                    //Member is suspend
                                    $endChargingDate = time() + 24 * 3600 * $totalDayBonus;
                                }
                                $endChargingDate = date('YmdHis', $endChargingDate);
                                $sqlUpdateMember = "
                                                        UPDATE member
                                                        SET status=1,start_charging_date='{$startChargingDate}',
                                                            end_charging_date='{$endChargingDate}',total_money=total_money+{$shortcodePrice},
                                                            total_try_charging=0
                                                        WHERE id='{$member['id']}'
                                                    ";
                                echo $sqlUpdateMember;
                                while (1) {
                                    try {
                                        $conn->doUpdate($sqlUpdateMember);
                                        break;
                                    } catch (Exception $e) {
                                        $errorMsg = "\n Update bonous error|102|{$data['UserID']}|" . date("Y-m-d H:i:s", time());
                                        file_put_contents($realPath . "log/{$mod}.txt", $errorMsg);
                                        try {
                                            $conn = new sbMssqlADO($server, $user, $password, $db);
                                        } catch (Exception $e) {
                                            file_put_contents($realPath . "log/{$mod}.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
                                            sleep(1);
                                        }
                                    }
                                }
                            } else {
                                //Register for member
                                $registerDate = $startChargingDate = date("YmdHis", time());
                                $endChargingDate = time() + 24 * 3600 * $totalDayBonus;
                                $endChargingDate = date('YmdHis', $endChargingDate);
                                $sqlInsertMember = "
                                                         INSERT INTO member(
                                                                                msisdn,service_id,register_date,
                                                                                start_charging_date,end_charging_date,status,
                                                                                total_money
                                                                            )
                                                         VALUES(
                                                                    '{$data['UserID']}','{$service['id']}','{$registerDate}',
                                                                    '{$startChargingDate}','{$endChargingDate}',1,
                                                                    '{$shortcodePrice}'
                                                                )
                                                   ";
                                echo "\n" . $sqlInsertMember;
                                while (1) {
                                    try {
                                        $conn->doUpdate($sqlInsertMember);
                                        break;
                                    } catch (Exception $e) {
                                        $errorMsg = "\n Register error|101|{$data['UserID']}|" . date("Y-m-d H:i:s", time());
                                        file_put_contents($realPath . "log/{$mod}.txt", $errorMsg);
                                        try {
                                            $conn = new sbMssqlADO($server, $user, $password, $db);
                                        } catch (Exception $e) {
                                            file_put_contents($realPath . "log/{$mod}.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
                                            sleep(1);
                                        }
                                    }
                                }
                                $sqlSelectMember = " SELECT * FROM member WHERE msisdn='{$data['UserID']}' AND service_id='{$service['id']}' AND status=1 ";
                                $member = $conn->doSelectOne($sqlSelectMember);
                                echo "\n Member value";
                                var_dump($member);
                            }
                            echo "\n Insert charging history";
                            //Insert charging history
                            $sqlInsertChargingHistory = "
                                                            INSERT INTO charging_history(
                                                                        member_id,start_charging_date,end_charging_date,
                                                                        total_money,service_id,mo_id,service_name
                                                                        )
                                                            VALUES(
                                                                    '{$member['id']}','{$startChargingDate}','{$endChargingDate}',
                                                                    '{$shortcodePrice}','{$member['service_id']}','{$data['ID']}','{$service['service']}'
                                                                  )
                                                        ";
                            $conn->doUpdate($sqlInsertChargingHistory);
                        } else {
                            //Not found service
                        }
                    } else {
                        //Not found service id code
                    }
                    usleep(500000);
                    $sqlUpdateMo = "Update SMS_RECV SET status=1 WHERE ID={$data['ID']}";
                    echo $sqlUpdateMo;
                    $conn->doUpdate($sqlUpdateMo);
                }
            } else {
                echo "\n Not found";
                break;
                sleep(1);
            }
        }
    }
}

function shortContent($str, $lenght) {
    return substr($str, 0, strripos(substr($str, 0, $lenght), " "));
}

function explodeContent($content) {
    $resutls = array();
    $contents = explode("#", $content);
    foreach ($contents as $content) {
        if (strlen($content) <= 140) {
            $results[] = $content;
        } else {
            while (1) {
                $shortContent = shortContent($content, 140);
                $results[] = $shortContent;
                $content = substr($content, strlen($shortContent) + 1);
                if (strlen($content) < 140) {
                    $results[] = $content;
                    break;
                }
            }
        }
    }
    return $results;
}

function checkSubtype($msisdn) {
    global $_vasgateway;
    $postVar = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vas="http://www.unitel.com.la/vasgateway/">
		   <soapenv:Header/>
		   <soapenv:Body>
			  <vas:Input>
				 <username>' . $_vasgateway['username'] . '</username>
				 <password>' . $_vasgateway['password'] . '</password>
				 <wscode>viewinfo</wscode>
				 <!--1 or more repetitions:-->
				<param name="GWORDER" value="1"/>
				<param name="input" value="' . $msisdn . '"/>
			  </vas:Input>
		   </soapenv:Body>
		</soapenv:Envelope>';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
        CURLOPT_URL => $_vasgateway['address'],
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects {$channelInfo['static_name']}
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $postVar,
        CURLOPT_HTTPHEADER => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: ' . strlen($postVar)),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array($ch, $options);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    $responeContent = curl_exec($ch);
    var_dump($responeContent);
    $result = 0;
    if ($err || !$responeContent) {
        $result[0] = 201;
    } else {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>') + 8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));

        $responeContent = explode('|', $responeContent);
        $result = $responeContent[0];
    }
    return $result;
}

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float) $usec + (float) $sec);
}

?>

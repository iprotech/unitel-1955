<?php
require('lib/sbMysqlPDO.class.php');
require('lib/config.php');

try {
    $conn = new sbMysqlPDO($db[0]['host'], $db[0]['user'], $db[0]['pass'], $db[0]['base']);
} catch (Exception $e) {
    
}

$Time_Created = date('Y-m-d', time() - 24 * 60 * 60);
//$Time_Created = '2014-02-04';
$sqlMember = 'SELECT member_id,sum(score) as total_score FROM `member_score` WHERE date(created_datetime)="' . $Time_Created . '" GROUP BY member_id';
$Total_Member = $conn->doSelect($sqlMember);
if (!$Total_Member) {
    exit;
}
foreach ($Total_Member as $item) {
    $sqlGetMsisdn = 'SELECT msisdn FROM member WHERE id=' . $item['member_id'];
    $Member = $conn->doSelectOne($sqlGetMsisdn);

    $sqlInsert = 'INSERT INTO report_score2014(
                                            msisdn,score,created_datetime
                                        ) VALUES(
                                            "' . $Member['msisdn'] . '",' . $item['total_score'] . ',"' . $Time_Created . '"
                                        )';
    $conn->doUpdate($sqlInsert);
}
?>

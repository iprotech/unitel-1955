<?php

error_reporting(false);
require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');

$mysql = new Mysql($db);

$questions = $mysql->ExecuteQuery('SELECT id, file_path_question, file_path_choose FROM question WHERE convert_status = 0 ORDER BY id ASC LIMIT 10');
if (isset($questions) && $questions) {
    foreach ($questions as $question) {
        $fileOrgPath = $_math_file . "quiz/mp3/" . $question['file_path_question'];
        $fileMp3 = $_math_file . "quiz/mp3/temp_" . $question['file_path_question'];
        $fileDestPath = $_math_file . "quiz/mono/" . $question['file_path_question'];
        //Convert file
        exec("/usr/bin/ffmpeg -i $fileOrgPath -y -vn -ar 44100 -ac 2 -ab 192 -f  mp3 $fileMp3");
        //Rename
        unlink($fileOrgPath);
        rename($fileMp3, $fileOrgPath);
        usleep(1000);
        
        exec("/usr/bin/ffmpeg -i $fileOrgPath -y -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
        $fileName = getFileName($question['file_path_question']) . ".alaw";
        rename($_math_file . "quiz/mono/" . $question['file_path_question'], $_math_file . "quiz/mono/" . $fileName);
        usleep(1000);
        
        
        $fileOrgPath = $_math_file . "quiz/mp3/" . $question['file_path_choose'];
        $fileMp3 = $_math_file . "quiz/mp3/temp_" . $question['file_path_choose'];
        $fileDestPath = $_math_file . "quiz/mono/" . $question['file_path_choose'];
        //Convert file
        exec("/usr/bin/ffmpeg -i $fileOrgPath -y -vn -ar 44100 -ac 2 -ab 192 -f  mp3 $fileMp3");
        //Rename
        unlink($fileOrgPath);
        rename($fileMp3, $fileOrgPath);
        usleep(1000);
        
        exec("/usr/bin/ffmpeg -i $fileOrgPath -y -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
        $fileName = getFileName($question['file_path_choose']) . ".alaw";
        rename($_math_file . "quiz/mono/" . $question['file_path_choose'], $_math_file . "quiz/mono/" . $fileName);
        usleep(1000);

        $mysql->ExecuteQuery("UPDATE question SET convert_status = 1 WHERE id = " . $question['id']);
    }
}
?>

<?php

error_reporting(false);
require('lib/mysql.lib.php');
require('lib/functions.php');
require('lib/config.php');

$mysql = new Mysql($db);

$questions = $mysql->ExecuteQuery("SELECT id, file_upload, file_name FROM system_new WHERE convert_status = 0");
if (isset($questions) && $questions) {
    foreach ($questions as $question) {
        $fileOrgPath = $_math_file . "system01062014/mp3/" . $question['file_upload'];
        $fileMp3 = $_math_file . "system01062014/mp3/temp_" . $question['file_upload'];
        $fileDestPath = $_math_file . "system01062014/mono/" . $question['file_upload'];
        //Convert file
        exec("/usr/bin/ffmpeg -i $fileOrgPath -y -vn -ar 44100 -ac 2 -ab 192 -f  mp3 $fileMp3");
        exec("/usr/bin/ffmpeg -i $fileOrgPath -y -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
        //Rename
        rename($_math_file . "system01062014/mono/" . $question['file_upload'], $_math_file . "system01062014/mono/" . $question['file_name'] . '.alaw');
        unlink($fileOrgPath);
        rename($fileMp3, $fileOrgPath);
        $mysql->ExecuteQuery("UPDATE system_new SET convert_status = 1 WHERE id = " . $question['id']);
    }
}
?>
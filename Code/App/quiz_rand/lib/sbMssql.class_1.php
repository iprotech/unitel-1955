<?php

class sbMssql{
    private $conn;
   
    function __construct($hostname,$username,$password,$dbname){
        $this->conn = mssql_connect($hostname,$username,$password);
        if(!$this->conn){
            echo "Connect to mssql Server Error";
        }
        // Select Database
        if(!mssql_select_db($dbname)){
            echo "Connect to DB Error , DBName = $dbname";
        }
    }

    public function doSelect($sql){
        $results = array();
        $query = mssql_query($sql,$this->conn);
        // Check if there were any records
        if (!mssql_num_rows($query)) {
            echo 'No records found';
        }else{
            while ($row = mssql_fetch_array($query, MSSQL_NUM)) {
                $results[] = $row;
            }
        }
        // Free the query result
        mssql_free_result($query);
        return $results;
    }

    public function doUpdate($sqlUpdate){
        $insert = mssql_query($sqlUpdate,$this->conn);
        if(!$insert){
            // echo "<script>alert('Insert Data Error');</script>"; // English Error Message
            echo "INSERT ERROR".$sqlUpdate; // Chinese Error Message\
        }
        return;
    }

    function disconnect(){
        mssql_close($this->conn);
    }
}
?>

function getUrlText(strURL,cmd) {
	if(strURL.indexOf("?")<0)
		strURL+="?";
	else
		strURL+="&";
	strURL+="nowD="+(new Date());
    var xmlHttpReq;
    // Mozilla/Safari
    if (window.XMLHttpRequest) {
        xmlHttpReq = new XMLHttpRequest();
    } else if (window.ActiveXObject) {  // IE
		var _ar=[ "Microsoft.XMLHTTP", "Msxml2.XMLHTTP" ];
		for(i=0;i<_ar.length;i++) {
			try{
				//alert(_ar[i]);
				xmlHttpReq = new ActiveXObject(_ar[i]);
				if(xmlHttpReq!=null) break;
			}catch (err){}
		}
    }
    xmlHttpReq.open('GET',strURL,true);
    xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xmlHttpReq.onreadystatechange = function() {
        if (xmlHttpReq.readyState == 4) {
			var data=xmlHttpReq.responseText;
			if((""+data).indexOf("<MonitorForm>")!=-1) {
				window.location.reload();
			}else
				eval(cmd); 
        }
    }
    xmlHttpReq.send(strURL);
}

function $(o) {
	return document.getElementById(o);
}
// ------OnKeyUp or onKeyDown----------------------
function checkNum(obj,evt) {
	oldv=obj.value;
	newV='';
	snum='0123456789';
	ok=true;
	for(i=0;i<oldv.length;i++) {
		if(snum.indexOf(oldv.substring(i,i+1))>=0)
			newV+=oldv.substring(i,i+1);
		else
			ok=false;
	}
	obj.value=newV;
	return ok;
}

function checkStr(str,sok) {
	sok = 'abcdefghijklmnopqrstuvwxyz0123456789' + sok;
	str = str.toLowerCase();
	for(i=0;i<str.length;i++) {
		c=str.substring(i,i+1);
		if(sok.indexOf(c)==-1) return false;
	}
	return true;
}

function checkNumber(str) {
	sok = '0123456789+-.';
	str = str.toLowerCase();
	for(i=0;i<str.length;i++) {
		c=str.substring(i,i+1);
		if(sok.indexOf(c)==-1) return false;
	}
	return true;
}

var popObj=null;

function hiddenWin() {
	sobj='_pop_win';
	
	if(arguments[0]!=null) {
		sobj=""+arguments[0];
	}
	$(sobj).style.visibility='hidden';
	if(popObj!=null) {
		popObj.style.visibility="hidden";
		popObj.style.left=0;
		popObj.style.top=0;
		popObj=null;
	}
	document.body.style.overflow="auto";

}

function popWin(sHTML) {
	sobj='_pop_win';
	if(arguments[1]) {
		sobj=""+arguments[1];
	}
	//alert("popWin: "+ sobj);
	if($(sobj)==null) {
		_obj=document.createElement("div");
		_obj.id=sobj;
		_obj.style.left='0';
		_obj.style.top='0';
		_obj.style.width='100%';
		_obj.style.height='100%';
		_obj.style.zIndex='1';
		_obj.style.position='absolute';
		_obj.innerHTML="<div style='top:0;left:0;width:100%;height:100%;position:absolute;z-index:1;filter:alpha(opacity:60);opacity:0.6;background:gray;' id='bgr"+sobj+"'>&nbsp;</div>";
		_obj.innerHTML+="<table cellpadding='0' cellspacing='0' style='top:0;left:0;width:100%;height:100%;position:absolute;z-index:10;'><tr><td align='center' valign='middle' id='sHTML"+sobj+"'></td></tr></table>";
		document.body.appendChild(_obj);
	}
	$('bgr'+sobj).style.width=($(sobj).offsetWidth + document.body.scrollLeft)/(0.01*$(sobj).offsetWidth)+"%";
	$('bgr'+sobj).style.height=($(sobj).offsetHeight + document.body.scrollTop)/(0.01*$(sobj).offsetHeight)+"%";
	$('sHTML'+sobj).style.width=($(sobj).offsetWidth + document.body.scrollLeft)/(0.01*$(sobj).offsetWidth)+"%";
	$('sHTML'+sobj).style.height=($(sobj).offsetHeight + document.body.scrollTop)/(0.01*$(sobj).offsetHeight)+"%";

	$('sHTML'+sobj).innerHTML=sHTML;
	$(sobj).style.visibility='visible';
	document.body.style.overflow="hidden";
}

function popUp(_sobj) {
	popWin("");
	popObj=$(_sobj);
	popObj.style.left=document.body.scrollLeft;
	popObj.style.top=document.body.scrollTop;
	popObj.style.visibility="visible";
	
}

function checkAll(prefix,value) {
	i=0;
	while($(prefix+i)!=null) {
		$(prefix+i).checked=value;
		i++;
	}
} 

r=0;
function rowOn(vobj,ncls) {
	var ocls=vobj.className;
	vobj.className=ncls;
	if(vobj.id=='') {
		vobj.id='row_'+r;
		r++;
	}
	try { vobj.onmouseout=function(){ vobj.className=ocls; }; }catch (er){}
	try { vobj.setAttribute("onMouseOut","function(){ $('"+vobj.id+"').className='"+ocls+"'; };"); }catch (er){}
	try { vobj.setAttribute("onMouseOut","$('"+vobj.id+"').className='"+ocls+"';"); }catch (er){}
}

function openWindow(surl,sname,w,h) {
	 var ll=(window.screen.width-w)/2;
	 var hh=(window.screen.height-h)/2;	
	 var pop=window.open(surl,sname,"fullscreen=0,location=0,scrollbars=1,resizable=1,toolbar=0,menubar=0,top="+hh+",left="+ll+",width="+w+",height="+h);
	 pop.focus();
}

function openDialog(surl,stitle,w,h) {
	if($('_dialog')==null) {
		_obj=document.createElement("div");
		_obj.id='_dialog';
		_obj.style.left='0';
		_obj.style.top='0';
		_obj.style.width='100%';
		_obj.style.height='100%';
		_obj.style.zIndex='2';
		_obj.style.position='absolute';
		_obj.innerHTML="<table style='top:0;left:0;width:100%;height:100%;position:absolute;z-index:2;'><tr><td align='center' valign='middle'>"
							+" <table cellpadding='0' cellspacing='0' id='dialog_content' style='border:1px solid #0000FF;'>"
							+"  <tr style='background:#8080FF;color:#ffffff;text-size:14px;height:14px;'><td nowrap>"+stitle+"</td><td align='right'><input type='button' value='X' onclick='closeDialog();'></td></tr>"
							+"  <tr><td style='background:#FFFFFF;' colspan='2'><iframe src=\""+surl+"\" name='iframe_dialog' style='border-radius:5px;width:99%;height:99%;overflow:hidden;' id='iframe_dialog'></iframe></td></tr>"
							+" </table>"
							+"</td></tr></table>";
		document.body.appendChild(_obj);
	}
	$('dialog_content').style.width=parseInt(""+w,10)+"px";
	$('dialog_content').style.height=(parseInt(""+h,10)+14)+"px";
	popUp('_dialog');
}

function closeDialog() {
	try { hiddenWin(); }catch(er){}
	try { parent.hiddenWin(); }catch(er){}
}

function iWait() {
	sHTML="<table bgcolor='green'><tr><td class='i_wait'>please wait...</table>";
	 popWin(sHTML,'_iwait');
}
function hWait() {
	hiddenWin('_iwait');
}

var URL=function(_uri) {
	this.uri=_uri;
	this.query='';

	this.add=function(_name,_value) {
		if(this.query=='')
			this.query+="?" + _name + "=" + encodeURI(_value);
		else
			this.query+="&" + _name + "=" + encodeURI(_value);
	}

	this.get=function() {
		return this.uri + this.query;
	}
}

function cutStr(str,sstart,send,ix) {
	idx=parseInt(''+ix);
	if(idx<0) return "";
	if(str.length<idx) return "";
	str=str.substring(idx);
	if(str.indexOf(sstart)==-1) return "";
	return str.substring(str.indexOf(sstart)+sstart.length,str.indexOf(send));
}


/*
		Input: arConf: ['id','type','state'] ----> type: 'TEXTBOX', 'SELECT', 'CHECKBOX', 'MULTI_CHECK', 'HTML';--> state: '', 'nofil', 'noget'
						pref:  ----- prefix of Object. (object_id= prefix+id)
		fillData(str) -- (str for config xml (<{id}>{value}</{id}>
		getUrl    --- Result URL Object()
*/
var FullData=function(_arConf,_pref) {
	
	this.arConf=_arConf;
	this.prefix=_pref;
	
	this.fillData=function(str) {
		
		for(i=0;i<this.arConf.length;i++) {
			vid=this.arConf[i][0];
			vtype=this.arConf[i][1];
			vstate='';
			try{ vstate=this.arConf[i][2]; }catch(err){}
			vvalue=cutStr(str,"<"+vid+">","</"+vid+">",0);
			
			if(vtype=='TEXTBOX') {
				$(this.prefix+vid).value='';
				if(vstate!='nofil') 
					$(this.prefix+vid).value=vvalue;	
			} 
			if(vtype=='CHECKBOX') {
				$(this.prefix+vid).checked=false;
				if(vstate!='nofil')
					$(this.prefix+vid).checked=($(this.prefix+vid).value==vvalue);
			}
			if(vtype=='SELECT') {
				options=$(this.prefix+vid).options;
				options[0].selected=true;
				if(vstate!='nofil') {
					for(x=0;x<options.length;x++) {
						options[x].selected=(options[x].value==vvalue);
					}
				}
			}
			if(vtype=='MULTI_CHECK') {
				ix=0;
				while($(this.prefix+vid+ix)!=null) {
					$(this.prefix+vid+ix).checked=false;
					if(vstate!='nofil') {
						if(vvalue.indexOf(",")==-1) 
							$(this.prefix+vid+ix).checked=($(this.prefix+vid+ix).value==vvalue);
						else 
							$(this.prefix+vid+ix).checked=((","+vvalue+",").indexOf(","+$(this.prefix+vid+ix).value+",")>=0);
					}
					ix++;
				}
			}
			if(vtype=='HTML') {
				$(this.prefix+vid).innerHTML='';
				if(vstate!='nofil')
					$(this.prefix+vid).innerHTML=vvalue;
			}
		}
	}
	
	this.getUrl=function(_url) {
		surl=new URL(_url);
		for(i=0;i<this.arConf.length;i++) {
			vid=this.arConf[i][0];
			vtype=this.arConf[i][1];
			vstate='';
			if(vstate=='noget') continue;
			try{ vstate=this.arConf[i][2]; }catch(err){}
			if((vtype=='TEXTBOX')||(vtype=='SELECT')) {
				surl.add(vid,$(this.prefix+vid).value);
			} else {
				if(vtype=='HTML') surl.add(vid,$(this.prefix+vid).innerHTML);
				if((vtype=='CHECKBOX')&&($(this.prefix+vid).checked)) {
					surl.add(vid,$(this.prefix+vid).value);
				}
				if(vtype=='MULTI_CHECK') {
					skq='';
					i=0;
					while($(this.prefix+vid+i)!=null) {
						if($(this.prefix+vid+i).checked) skq+=$(this.prefix+vid+i).value+",";
						i++;
					}
					if(skq.length>0) skq=skq.substring(0,skq.length-1);
					surl.add(vid,skq);
				}
			}		
		}
		return surl;
	}
	
	
}

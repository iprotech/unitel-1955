-- MySQL dump 10.11
--
-- Host: localhost    Database: unitel_horoscope
-- ------------------------------------------------------
-- Server version	5.0.95-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(45) default NULL,
  `password` varchar(128) default NULL,
  `group_id` int(10) default NULL,
  `module` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cdr`
--

DROP TABLE IF EXISTS `cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr` (
  `id` int(11) NOT NULL auto_increment,
  `start` datetime NOT NULL default '0000-00-00 00:00:00',
  `callerid` varchar(80) NOT NULL default '',
  `src` varchar(80) NOT NULL default '',
  `dst` varchar(80) NOT NULL default '',
  `dcontext` varchar(80) NOT NULL default '',
  `channel` varchar(80) NOT NULL default '',
  `dstchannel` varchar(80) NOT NULL default '',
  `lastapp` varchar(80) NOT NULL default '',
  `lastdata` varchar(80) NOT NULL default '',
  `duration` int(11) NOT NULL default '0',
  `billsec` int(11) NOT NULL default '0',
  `disposition` varchar(45) NOT NULL default '',
  `amaflags` int(11) NOT NULL default '0',
  `accountcode` varchar(1024) character set utf8 default NULL,
  `userfield` varchar(255) NOT NULL default '',
  `uniqueid` varchar(255) NOT NULL default '',
  `status` int(11) default '0',
  `inform_status` int(11) default '0',
  PRIMARY KEY  (`id`),
  KEY `inform_status` (`inform_status`)
) ENGINE=MyISAM AUTO_INCREMENT=6000823 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cdr_report`
--

DROP TABLE IF EXISTS `cdr_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr_report` (
  `msisdn` int(11) default NULL,
  KEY `msisdn` (`msisdn`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `charging_history`
--

DROP TABLE IF EXISTS `charging_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charging_history` (
  `id` int(10) NOT NULL auto_increment,
  `member_id` int(10) NOT NULL,
  `start_charging_date` datetime NOT NULL,
  `end_charging_date` datetime NOT NULL,
  `total_money` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `mo_id` int(10) NOT NULL,
  `service_name` varchar(255) collate utf8_unicode_ci NOT NULL,
  `charging_type` int(10) default NULL,
  `convert_status` tinyint(2) default NULL,
  PRIMARY KEY  (`id`),
  KEY `member_id` (`member_id`),
  KEY `start` (`start_charging_date`),
  KEY `end` (`end_charging_date`)
) ENGINE=MyISAM AUTO_INCREMENT=1198139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` text,
  `file_path` varchar(255) default NULL,
  `status` tinyint(2) default '1',
  `convert_status` tinyint(2) default '1',
  `created` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horoscope_advice`
--

DROP TABLE IF EXISTS `horoscope_advice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horoscope_advice` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(256) default NULL,
  `status` int(11) default NULL,
  `type` int(4) default NULL,
  `file_path` varchar(256) default NULL,
  `created_datetime` datetime default NULL,
  `start_play` date default NULL,
  `end_play` date default NULL,
  `convert_status` int(4) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horoscope_augur`
--

DROP TABLE IF EXISTS `horoscope_augur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horoscope_augur` (
  `id` int(11) NOT NULL auto_increment,
  `number` int(4) default NULL,
  `status` int(4) default NULL,
  `file_path` varchar(128) default NULL,
  `created_datetime` datetime default NULL,
  `convert_status` int(4) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horoscope_daily`
--

DROP TABLE IF EXISTS `horoscope_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horoscope_daily` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(256) default NULL,
  `day_in_week` int(11) default NULL COMMENT '2. Monday, 3.Tuesday, 4.Wenesday, 5.Thursday, 6.Friday, 7.Saturday, 8.Sunday',
  `status` int(11) default NULL,
  `file_path` varchar(256) default NULL,
  `created_datetime` datetime default NULL,
  `start_play` date default NULL,
  `end_play` date default NULL,
  `convert_status` int(4) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horoscope_dream`
--

DROP TABLE IF EXISTS `horoscope_dream`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horoscope_dream` (
  `id` int(11) NOT NULL auto_increment,
  `name` int(4) default NULL,
  `status` int(4) default NULL,
  `file_path` varchar(128) default NULL,
  `created_datetime` datetime default NULL,
  `convert_status` int(4) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horoscope_month`
--

DROP TABLE IF EXISTS `horoscope_month`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horoscope_month` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(256) default NULL,
  `status` int(11) default NULL,
  `file_path` varchar(256) default NULL,
  `created_datetime` datetime default NULL,
  `start_play` date default NULL,
  `end_play` date default NULL,
  `convert_status` int(4) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horoscope_week`
--

DROP TABLE IF EXISTS `horoscope_week`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horoscope_week` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(256) default NULL,
  `status` int(11) default NULL,
  `file_path` varchar(256) default NULL,
  `created_datetime` datetime default NULL,
  `start_play` date default NULL,
  `end_play` date default NULL,
  `convert_status` int(4) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horoscope_year2012`
--

DROP TABLE IF EXISTS `horoscope_year2012`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horoscope_year2012` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(256) default NULL,
  `status` int(11) default NULL,
  `file_path` varchar(256) default NULL,
  `created_datetime` datetime default NULL,
  `convert_status` int(4) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lucky_code`
--

DROP TABLE IF EXISTS `lucky_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lucky_code` (
  `id` int(11) NOT NULL auto_increment,
  `msisdn` varchar(16) NOT NULL,
  `member_id` int(11) NOT NULL,
  `register_date` datetime NOT NULL,
  `cancel_date` datetime NOT NULL,
  `sms_status` int(4) NOT NULL,
  `sms_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102379 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` int(10) NOT NULL auto_increment,
  `msisdn` varchar(50) collate utf8_unicode_ci NOT NULL,
  `birthday` date default NULL,
  `day_of_week` int(4) default NULL COMMENT '1. Monday, 2. Tu, 3 Wes, ',
  `msisdn_error` varchar(16) collate utf8_unicode_ci NOT NULL,
  `service_id` int(10) NOT NULL,
  `register_date` datetime default NULL,
  `start_free_date` datetime default NULL,
  `end_free_date` datetime default NULL,
  `start_charging_date` datetime default NULL,
  `end_charging_date` datetime default NULL,
  `status` tinyint(2) NOT NULL,
  `total_try_charging` int(10) NOT NULL,
  `total_money` int(10) NOT NULL,
  `cancel_date` datetime default NULL,
  `convert_status` tinyint(2) default NULL,
  `register_type` int(4) default '0',
  `inform_status` int(11) default '0',
  `charging_status` int(2) default '0',
  PRIMARY KEY  (`id`),
  KEY `msisdn` (`msisdn`),
  KEY `register` (`register_date`),
  KEY `start` (`start_charging_date`),
  KEY `end` (`end_charging_date`)
) ENGINE=MyISAM AUTO_INCREMENT=573085 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member_log`
--

DROP TABLE IF EXISTS `member_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_log` (
  `id` int(11) NOT NULL auto_increment,
  `msisdn` varchar(16) default NULL,
  `ivr_branh` varchar(256) default NULL,
  `press_key` int(11) default NULL,
  `next_scenario` varchar(256) default NULL,
  `created_datetime` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4033451 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member_status`
--

DROP TABLE IF EXISTS `member_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_status` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL auto_increment,
  `new_register` int(11) default NULL,
  `new_register_sms` int(11) default NULL,
  `new_register_ivr` int(11) default NULL,
  `cancel_by_user` int(11) default NULL,
  `cancel_by_system` int(11) default NULL,
  `total_cancel` int(11) default NULL,
  `real_development` int(11) default NULL,
  `current_user` int(11) default NULL,
  `charge_able` int(11) default NULL,
  `total_revenue` int(11) default NULL,
  `report_date` date default NULL,
  `new_register_auto` int(10) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=477 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_1104`
--

DROP TABLE IF EXISTS `report_1104`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_1104` (
  `msisdn` bigint(16) default NULL,
  KEY `msisdn` (`msisdn`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_1121`
--

DROP TABLE IF EXISTS `report_1121`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_1121` (
  `msisdn` int(11) NOT NULL default '0',
  PRIMARY KEY  (`msisdn`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_1209`
--

DROP TABLE IF EXISTS `report_1209`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_1209` (
  `msisdn` int(11) default NULL,
  KEY `msisdn` (`msisdn`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_tmp`
--

DROP TABLE IF EXISTS `report_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_tmp` (
  `msisdn` varchar(16) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_mo`
--

DROP TABLE IF EXISTS `sms_mo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_mo` (
  `id` int(11) NOT NULL auto_increment,
  `short_code` int(11) default NULL,
  `msisdn` varchar(16) default NULL,
  `command_code` varchar(64) default NULL,
  `content` varchar(512) default NULL,
  `status` int(4) default NULL COMMENT '0. Pending, 1. Proceessed',
  `created_datetime` datetime default NULL,
  `updated_datetime` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=484806 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_mt`
--

DROP TABLE IF EXISTS `sms_mt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_mt` (
  `id` int(11) NOT NULL auto_increment,
  `short_code` int(11) default NULL,
  `msisdn` varchar(16) default NULL,
  `status` int(4) default NULL,
  `type` int(4) default NULL COMMENT '1. text; 2. wappush',
  `content` varchar(512) default NULL,
  `created_datetime` datetime default NULL,
  `updated_datetime` datetime default NULL,
  `phonenumber` varchar(16) default NULL,
  PRIMARY KEY  (`id`),
  KEY `status` (`status`),
  KEY `date` (`created_datetime`)
) ENGINE=MyISAM AUTO_INCREMENT=3492107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_mt_backup_20140703`
--

DROP TABLE IF EXISTS `sms_mt_backup_20140703`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_mt_backup_20140703` (
  `id` int(11) NOT NULL auto_increment,
  `short_code` int(11) default NULL,
  `msisdn` varchar(16) default NULL,
  `status` int(4) default NULL,
  `type` int(4) default NULL COMMENT '1. text; 2. wappush',
  `content` varchar(512) default NULL,
  `created_datetime` datetime default NULL,
  `updated_datetime` datetime default NULL,
  `phonenumber` varchar(16) default NULL,
  PRIMARY KEY  (`id`),
  KEY `status` (`status`),
  KEY `date` (`created_datetime`)
) ENGINE=MyISAM AUTO_INCREMENT=2967479 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sub_cancel_0730`
--

DROP TABLE IF EXISTS `sub_cancel_0730`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_cancel_0730` (
  `id` int(11) NOT NULL auto_increment,
  `msisdn` bigint(20) default NULL,
  `code` varchar(64) default NULL,
  `status` int(4) default NULL,
  PRIMARY KEY  (`id`),
  KEY `msisdn` (`msisdn`)
) ENGINE=MyISAM AUTO_INCREMENT=215191 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_active`
--

DROP TABLE IF EXISTS `tmp_active`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_active` (
  `msisdn` int(11) NOT NULL default '0',
  PRIMARY KEY  (`msisdn`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_cancel`
--

DROP TABLE IF EXISTS `tmp_cancel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_cancel` (
  `msisdn` int(11) NOT NULL default '0',
  PRIMARY KEY  (`msisdn`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vas_report`
--

DROP TABLE IF EXISTS `vas_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vas_report` (
  `msisdn` varchar(16) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vas_report_a`
--

DROP TABLE IF EXISTS `vas_report_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vas_report_a` (
  `msisdn` varchar(16) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-16 11:49:42
